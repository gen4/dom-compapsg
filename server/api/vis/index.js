'use strict'

var express = require('express')
var auth = require('../../auth/auth.service')
var visCtrl = require('./vis.controller')
var uploadHelper = require('../../helpers/uploadHelper')
var userCtrl = require('../user/user.controller')

var router = express.Router()

// project

router.get('/', auth.hasRole('admin'), visCtrl.list)
router.get('/:id', auth.hasRole('admin'), visCtrl.get)
// router.delete('/:id', auth.isAuthenticated(), visCtrl.remove)
// for admin
router.patch('/:id', auth.hasRole('admin'), visCtrl.update)
// router.put('/:id', auth.isAuthenticated(), controller.update)

router.post('/:id/uploads', auth.isAuthenticated(), userCtrl.isDeadlineMissed, auth.onlyVisOwner, uploadHelper.upload, visCtrl.addUploads)
router.delete('/:id/uploads/:uploadId', auth.isAuthenticated(), userCtrl.isDeadlineMissed, auth.onlyVisOwner, visCtrl.removeUploads)
// for users
router.put('/:id/uploads', auth.isAuthenticated(), userCtrl.isDeadlineMissed, uploadHelper.upload, visCtrl.updateUploads)

module.exports = router
