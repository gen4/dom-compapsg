'use strict'

var _ = require('lodash')
var Promise = require('bluebird')
var fs = require('fs')
var path = require('path')
var Vis = require('./vis.model')
var Project = require('../project/project.model')
var Upload = require('../uploads/upload.model')
var CommonRestHelper = require('../../helpers/restHelper')
var config = require('../../../config').backend
var handleErrorAsync = require('../errorhandler')
var s3 = require('../../helpers/uploadHelper').s3;
const rest = new CommonRestHelper(Vis)


const update = {
  addUploads: function (req, res, next) {
    const { id } = req.params
    Vis.getWithTemplate(id)
      .then((vis) => {
        if (vis.uploads.length >= vis.filesLimit) {
          return Promise.reject(new Error('max files uploaded'))
        }
        const f = req.files || req.file
        if (f && f[0] && (f[0].filename || f[0].location)) {
          const url = f[0].path ? f[0].path.split(config.uploadDir)[1] : _.replace(f[0].location, 'http://', 'https://')

          const upload = new Upload({
            url: url, // req.files[0].filename,
            s3Url: _.replace(f[0].location, 'http://', 'https://'),
            localUrl: f[0].path,
            state: req.ms3 ? 'idle' : 'transferred',
            project: vis.project
          })
          return upload.save()
            .then((saved) => {
              return Vis.findByIdAndUpdate(id, { $addToSet: { uploads: saved }})
                .then(() => saved)
            })
        } else {
          return Promise.reject('No File')
        }
      })
      .then((upload) => {
        return res.json(upload)
      })
      .catch(handleErrorAsync(res))
  },
  updateUploads: function (req, res, next) {
    const { id } = req.params
    Vis.findByIdAndUpdate(id, { uploads: req.body, state: req.ms3 ? 'idle' : 'transferred' })
      .then(() => {
        return res.json({})
      })
      .catch(handleErrorAsync(res))
  },
  removeUploads: function (req, res, next) {
    const { id, uploadId } = req.params
    Vis.findByIdAndUpdate(id, { $pull: { uploads: uploadId }})
      .then(() => {
        return Upload.findById(uploadId)
          .select('+localUrl +s3Url')
          .lean()
      })
      .then((upload) => {
        console.log(upload)
        const tasks = []
        if (upload.s3Url) {
          tasks.push(deleteS3Object(upload.s3Url))
        }
        if (upload.localUrl) {
          tasks.push(deleteLocalFile(upload.localUrl))
        }
        return Promise.all(tasks)
      })
      .then(() => {
        return Upload.findByIdAndRemove(uploadId)
      })
      .then(() => {
        return res.json({})
      })
      .catch(handleErrorAsync(res))
  }
}

function deleteS3Object (url) {
  return Promise.resolve()
  // return new Promise((resolve, reject) => {
  //   s3.deleteObject({
  //     Bucket: process.env.S3_BUCKET,
  //     Key: `/public${url.split('/public')[1]}`
  //   }, function (err, data) {
  //     console.log('SSS', `/public${url.split('/public')[1]}`, err, data)
  //     if (err) {
  //       return reject(err)
  //     }
  //     return resolve(data)
  //   })
  // })
}

function deleteLocalFile () {
  return Promise.resolve()
}

module.exports = _.assign(rest, update)
