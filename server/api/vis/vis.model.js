const mongoose = require('mongoose')
const _ = require('lodash')
var config = require('../../../config').backend
const Promise = require('bluebird')
const Upload = require('../uploads/upload.model')
const Project = require('../project/project.model')
const path = require('path')
const Schema = mongoose.Schema
var createDirSync = require('../../helpers/filesystem')
const sharp = require('sharp')
const templates = require('../../config/initials/projects')

const visSchema = new Schema({
  // template is hot (we set it while populatiuon)
  name: { // using for custom proj vis
    ru: { type: String },
    en: { type: String }
  },
  __v: { type: Number, select: false },
  key: { type: String, index: true },
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
  project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project', index: true },
  uploads: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Upload' }]
})

visSchema.statics.getWithTemplate = function (id) {
  return this.findById(id)
    .lean()
    .then((vis) => {
      return _.assign({}, vis, templates.vis[vis.key])
    })
}

visSchema.statics.convert = function (id) {
  return this.findById(id)
    .lean()
    .populate({
      path: 'uploads',
      options: { lean: true }
    })
    .populate({
      path: 'project',
      select: '+id6',
      options: { lean: true }
    })
    .then((vis) => {
      return Promise.map(vis.uploads, (upload) => {
        // convert
        // create new upload
        const filename = _.startsWith(upload.url, 'http') ? upload.url : upload.url

        const newPath = vis.project.id6 || vis.project._id
        const rnd = Math.random().toString(36).substring(7)
        const ext = _.takeRight(filename.split('.'))
        const fullPath = path.resolve(config.uploadDir, _.trim(filename, '/'))
        const newFullPath = createDirSync(path.resolve(config.uploadDir, newPath))
        const newFileName = `${vis.project.id6 || vis.project._id}_vis_${rnd}${Date.now()}.${ext}`
        return sharp(fullPath)
          .resize(2000, 2000)
          .max()
          .toFormat('jpeg')
          .toFile(path.resolve(newFullPath, newFileName))
          .then((data) => {
            const newUpload = new Upload({
              url: `${newPath}/${newFileName}`,
              localUrl: `${newPath}/${newFileName}`,
              state: 'transferred',
              project: vis.project,
              keyPath: upload.keyPath
            })
            return newUpload.save()
          })
          .catch((err) => {
            return Promise.resolve(null)
          })
      })
        .then((uploads) => {
          const newVis = new this({
            name: vis.name,
            key: vis.key,
            user: vis.user,
            project: vis.project,
            uploads: _.compact(uploads)
          })
          return newVis.save()
        })
    })
}
// visSchema.statics.removeWithUpload = function (visId) {
//   return this.findById(visId)
//         .lean()
//         .then((vis) => {
//           return Promise.all([
//             this.findByIdAndRemove(vis._id),
//             Upload.findByIdAndRemove(vis.uploads[0])
//           ])
//             .then(() => {
//               return vis
//             })
//         })
// }

const ModelClass = mongoose.model('Vis', visSchema)
module.exports = ModelClass
