module.exports = function (res) {
  return function (err) {
    console.error(err)
    return res.status(500).send(err)
  }
}
