/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var Checker = require('./checker.model')
var handleError = require('../errorhandler')
// Get list of sids
exports.index = function (req, res) {
  Checker.find({})
    .lean()
    .then(function (sids) {
      return res.status(200).json({ results: sids })
    })
    .catch(handleError)
}
