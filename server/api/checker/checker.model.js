'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var SidSchema = new Schema({
  _id: { type: String, required: true, index: 'text' },
  type: { type: String, enum: ['num', 'bol', 'tri'] },
  isActive: { type: Boolean, default: true },
  isInverted: { type: Boolean, default: false},
  grades: {
    l: [{ type: Number }],
    m: [{ type: Number }],
    h: [{ type: Number }]
  }
}, { _id: false })

module.exports = mongoose.model('Checker', SidSchema)
