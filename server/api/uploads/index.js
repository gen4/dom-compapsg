'use strict'

var express = require('express')
var auth = require('../../auth/auth.service')
var uploadCtrl = require('./upload.controller')
var uploadHelper = require('../../helpers/uploadHelper')
var router = express.Router()
// project
// uploadCtrl.applyRoutes('/uploads', router)
router.get('/', auth.hasRole('admin'), uploadCtrl.list)
router.get('/:id', auth.hasRole('admin'), uploadCtrl.get)
router.post('/:id', auth.hasRole('admin'), uploadHelper.upload, uploadCtrl.update)
router.delete('/:id/clear', auth.hasRole('admin'), uploadCtrl.remove)
router.delete('/:id', auth.hasRole('admin'), uploadCtrl.remove)

module.exports = router
