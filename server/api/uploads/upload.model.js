const mongoose = require('mongoose')
const _ = require('lodash')
const Schema = mongoose.Schema

const UPLOAD_STATUSES = {
  IDLE: 'idle',
  SCHEDULED: 'scheduled',
  IN_TRANSFER: 'inTransfer',
  TRANSFERRED: 'transferred',
  ERROR: 'error'
}

const errorSchema = new Schema({
  timestamp: { type: Date, default: Date.now },
  text: { type: String }
}, { _id: false })

const UploadSchema = new Schema({
  url: { type: String }, // current Url to use
  s3Url: { type: String, select: false },
  __v: { type: Number, select: false },
  localUrl: { type: String, select: false },
  state: { type: String, enum: _.values(UPLOAD_STATUSES), default: UPLOAD_STATUSES.IDLE, select: false, index: true },
  startTransferDate: { type: Number, default: 0, select: false }, // timestamp
  project: { type: mongoose.Schema.Types.ObjectId, ref: 'Project', select: false, index: true },
  keyPath: { type: String, index: true },
  errs: [errorSchema]
})

UploadSchema.statics.markAsScheduledForDownload = function (uploadId, keyPath) {
  // return this.findOne({ _id: uploadId, state: UPLOAD_STATUSES.IDLE })
  return this.findOneAndUpdate({ _id: uploadId, state: UPLOAD_STATUSES.IDLE },
    {
      state: UPLOAD_STATUSES.SCHEDULED,
      keyPath
    })
}

UploadSchema.statics.markAsErrored = function (uploadId, error) {
  console.log('[UL] mark.ERROR')
  return this.findByIdAndUpdate(uploadId, {
    state: UPLOAD_STATUSES.ERROR,
    $push: {
      errs: {
        timestamp: new Date().getTime(),
        text: error
      }
    }
  })
}

const ModelClass = mongoose.model('Upload', UploadSchema)
module.exports = ModelClass
