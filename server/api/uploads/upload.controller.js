'use strict'

var _ = require('lodash')
var Promise = require('bluebird')
var fs = require('fs')
var path = require('path')
var Upload = require('./upload.model')
var CommonRestHelper = require('../../helpers/restHelper')
var config = require('../../../config').backend
var handleErrorAsync = require('../errorhandler')

const rest = new CommonRestHelper(Upload)

function createArrayUploads (projectId, files) {
  return Promise.map(files, (file) => createOneUpload(projectId, file))
}

function createOneUpload (projectId, file) {
  const upload = new Upload({
    localUrl: file.filename,
    url: file.filename,
    project: projectId,
  })
  return upload.save()
}

const update = {
  update: function (req, res, next) {
    // const projectId = req.params.projectId
    let url = req.body.url
    if (req.files && req.files[0] && req.files[0].filename) {
      console.log('z', url)
      url = `${req.files[0].filename}`
    }
    console.log(' upl upd', url)
    Upload.findByIdAndUpdate(req.params.id, { url }, { new: true })
      .then(function (saved) {
        return res.json(saved)
      })
      .catch(handleErrorAsync(res))
  },
  create: function (req, res, next) {
    const projectId = req.params.projectId
    if (req.files && req.files[0] && req.files[0].filename) {
      console.log('[uploader] Mutiple files')
      return createArrayUploads(projectId, req.files)
        .then((uploads) => {
          return res.json(uploads)
        })
        .catch(handleErrorAsync(res))
    } else if (req.file && req.file.filename) {
      console.log('[uploader] one file')
      return createOneUpload(projectId, req.file)
        .then((upload) => {
          return res.json(upload)
        })
        .catch(handleErrorAsync(res))
    }
    return res.status(400).json({ error: 'no file received' })
  },
  remove: function (req, res, next) {
    const { id } = req.params;
    Upload.findByIdAndRemove(id)
      .then(function (obj) {
        console.log('remove ', obj)
        if (obj.url) {
          const fullPath = path.resolve(config.uploadDir, obj.url)
          fs.unlinkSync(fullPath)
        }
        res.json(obj)
      })
      .catch(handleErrorAsync(res))
  }
}

module.exports = _.assign(rest, update);
