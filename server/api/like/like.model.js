'use strict'

var mongoose = require('mongoose')
var handleError = require('../errorhandler')
var handleErrorAsync = require('../../api/errorhandler')
var Schema = mongoose.Schema

var likeSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Project'
  },
  stage: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Stage'
  },
  value: { type: Number }
})

likeSchema.statics.findGroupLess = function () {
  console.log('find likes')
  return this.find()
    .populate({
      path: 'user',
      select: 'username',
      options: { lean: true }
    })
    .populate({
      path: 'project',
      select: 'user',
      populate: { path: 'user', select: 'viewName' },
      options: { lean: true }
    })
    .lean()
    .catch(handleErrorAsync(handleError))
}

likeSchema.statics.findGroup = function () {
  console.log('find likes')
  return this.find({})
  .lean()
    .then(() => {
      return this.aggregate([{
          $lookup: {
            from: 'users',
            localField: 'user',
            foreignField: '_id',
            as: 'voter'
          }
        }, {
          $unwind: '$voter'
        }, {
          $lookup: {
            from: 'users',
            localField: 'voter.username',
            foreignField: '_id',
            as: 'votername'
          }
        }, 
        {
          $lookup: {
            from: 'projects',
            localField: 'project',
            foreignField: '_id',
            as: 'project'
          }
        },
        {
          $unwind: '$project'
        }, {
          $lookup: {
            from: 'users',
            localField: 'project.user',
            foreignField: '_id',
            as: 'user'
          }
        }
      ])
    })
    .catch(handleErrorAsync(handleError))
}

// compound index
likeSchema.index({ 'user': 1, 'project': 1, 'stage': 1 }, { unique: true })

module.exports = mongoose.model('Like', likeSchema)
