'use strict'

var express = require('express')
var controller = require('./like.controller')
var auth = require('../../auth/auth.service')

var router = express.Router()

router.get('/', controller.index)
router.get('/group', auth.hasRole('admin'), controller.groupedList)
// router.get('/:id', controller.show)
// router.post('/', controller.make)
// router.put('/:id', controller.update)
// router.patch('/:id', controller.update)
// router.delete('/:id', controller.destroy)

module.exports = router
