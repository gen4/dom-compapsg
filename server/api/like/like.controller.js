/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

const voterNames = ['mutko', 'yakushev', 'plutnik', 'aleksakova', 'bosshardе', 'devries', 'lyashenko', 'ricciardi', 'tsimailo', 'burdova', 'goldhoorn', 'plotkin', 'shapiro', 'carnevali']

var _ = require('lodash')
var Like = require('./like.model')
var handleError = require('../errorhandler')
var handleErrorAsync = require('../../api/errorhandler')

// Get list of sids
exports.index = function (req, res) {
  Like.find({})
    .lean()
    .then(function (likes) {
      return res.status(200).json({
        results: likes
      })
    })
    .catch(handleError)
}

exports.groupedList = function (req, res) {
  return Like.findGroupLess()
    .then(function (likes) {
     // console.log(likes)
     var projectLikes = []
     likes.forEach(function(lk) {
      var existed = false
      if (projectLikes.length > 0) {
      projectLikes.forEach(function(pl) {
        if (lk.project._id === pl.projectId) {
          pl[lk.user.username] = lk.value
          existed = true
        }
      })
    }
      if (!existed) {
        var projectInfo = {"projectId": lk.project._id, "viewName": lk.project.user.viewName}
        projectInfo[lk.user.username] = lk.value
        projectLikes.push(projectInfo)
        console.log(projectInfo)
      }
     })
     console.log(projectLikes)
      return res.status(200).json({
        results: projectLikes.sort(function(a,b) {
         // console.log(a.viewName)
          if (a.viewName) {
            return a.viewName.localeCompare(b.viewName);
          } else {
            return false
          }
      })
      })
    })
    .catch(handleErrorAsync(handleError))
}
