const mongoose = require('mongoose')
const _ = require('lodash')
const Promise = require('bluebird')
const Schema = mongoose.Schema
const ObjectId = mongoose.Types.ObjectId;
const Upload = require('../uploads/upload.model')
const User = require('../user/user.model')
const System = require('../system/system.model')
const Stage = require('../stage/stage.model')
// const stageCtrl = require('../stage/stage.controller')
const Like = require('../like/like.model')
const Vis = require('../vis/vis.model')
const pdf = require('../../helpers/pdf')
const templates = require('../../config/initials/projects')
var config = require('../../../config').backend
var path = require('path')

// const visSchema = new Schema({
//   key: { type: String },
//   name: {
//     ru: { type: String },
//     en: { type: String }
//   },
//   isRequired: { type: Boolean },
//   image: { type: mongoose.Schema.Types.ObjectId, ref: 'Upload' }
// })

const tepSchema = new Schema({
  key: { type: String },
  value: { type: mongoose.Schema.Types.Mixed }
})

const subBlockSchema = new Schema({
  marked: { type: Boolean },
  key: { type: String },
  items: [tepSchema],
  vis: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Vis' }]
})

const blockSchema = new Schema({
  key: { type: String },
  items: [subBlockSchema]
})

const downloadSchema = new Schema({
  total: { type: Number, default: 0 },
  done: { type: Number, default: 0 },
  errs: { type: Number, default: 0 }
}, { _id: false })

const ProjectSchema = new Schema({
  __v: { type: Number, select: false },
  desc: {
    ru: { type: String, maxlength: 1300, trim: true },
    en: { type: String, maxlength: 1300, trim: true }
  },
  id6: { type: String, index: true, select: false },
  id4: { type: String, index: true, select: false },
  customLikes: { type: Number, index: true },
  rnd: { type: Number, index: true },
  album: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  albumConverted: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  panel1: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  panel1converted: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  panel2: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  panel2converted: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  albumPrint: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  panel1Print: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  panel2Print: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  cover: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  schemas: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  decl: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  consent: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  note: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  blueprint: { type: mongoose.Schema.Types.ObjectId, ref: 'Vis' },
  vis: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Vis' }],
  visConverted: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Vis' }],
  score: { type: Number, default: 0 },
  newScore: { type: Number, default: 0 },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    select: false
  },
  oldUser: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    select: false
  },
  items: [blockSchema],
  isSubmitted: { type: Boolean, default: false, select: false, index: true },
  isApproved: { type: Boolean, default: false, select: false, index: true },
  isFinalist: { type: Boolean },
  editionAllowedTill: { type: Date },
  peopleFav: { type: Boolean, select: false },
  editorFav: { type: Boolean, select: false },
  editorPrice: { type: String, enum: ['h', 'm', 'l'], select: false },
  editorTzFlat: { type: String, enum: ['h', 'm', 'l'], select: false },
  editorTzBuilding: { type: String, enum: ['h', 'm', 'l'], select: false },
  editorPlannedStructure: { type: String, enum: ['h', 'm', 'l'], select: false },
  editorEnginiringSystems: { type: String, enum: ['h', 'm', 'l'], select: false },
  readiness: { type: Number, index: true, select: false },
  readinessAdmin: { type: Boolean, index: true, select: false },
  csv: { type: Boolean, index: true, select: false },
  download: { type: downloadSchema, select: false }
})

const scoreValueMap = {
  0: 0,
  1: 8,
  2: 3,
  3: 1
}

const hiddenFields = ['id6', 'id4', 'user', 'isSubmitted', 'isApproved', 'peopleFav', 'editorFav', 'editorPrice', 'editorTzFlat', 'editorTzBuilding', 'editorPlannedStructure', 'editorEnginiringSystems', 'readiness', 'readinessAdmin', 'download', 'csv']
const hiddenFieldsPlus = _.reduce(hiddenFields, (t, f) => t + `+${f} `, '')

const baseVisFields = ['album', 'panel1', 'panel1converted', 'panel2', 'panel2converted', 'albumPrint', 'panel1Print', 'panel2Print', 'cover', 'schemas', 'decl', 'consent', 'note', 'blueprint']

ProjectSchema.statics.createEmptyWithUser = function (user) {
  const Model = this
  const pt = _.cloneDeep(templates.projects[0])
  let obj;

  return System.findOne({})
    .lean()
    .select('deadline')
    .then((sys) => {
      return User.findById(user._id)
        .select('+id4 +id6')
        .then((fullUser) => {
          return Model.findOne({ id6: fullUser.id6 })
            .lean()
            .then((existingProject) => {
              if (existingProject) {
                return Promise.reject('user already have a project')
              }
              return fullUser
            })
        })
        .then((fullUser) => {
          obj = _.assign({}, pt, {
            user: user,
            rnd: Math.random() * 100000,
            editionAllowedTill: sys.deadline,
            id4: fullUser.id4,
            id6: fullUser.id6
          })
          const project = new Model(removeTemplates(obj))
          return project.save()
        })
    })
    .then((savedProject) => {
      return seedAllInternalObjects(savedProject._id, user, obj)
        .then(() => populateProject(Model.findById(savedProject._id)), user._id)
    })
}

ProjectSchema.statics.getByUserId = function (userId) {
  return populateProject(this.findOne({ user: userId }), userId)
}

ProjectSchema.statics.addNewVis = function (projectId, user, pathInProject, data, isArray) {
  const visToSave = new Vis(_.assign(data, {
    project: projectId,
    user
  }))
  return visToSave.save()
    .then((vis) => {
      vis = vis.toObject()
      // console.log('+++ * ', pathInProject, vis)
      const template = pathInProject === 'visConverted' ? 'baseVis' : data.key
      if (isArray) {
        return this.findByIdAndUpdate(projectId, { $addToSet: { [pathInProject]: vis } })
          .then(() => {
            _.each(templates.vis[template], (v, k) => {
              vis[k] = v
            })
            return vis
          })
      }
      return this.findByIdAndUpdate(projectId, { [pathInProject]: vis })
        .then(() => {
          _.each(templates.vis[template], (v, k) => {
            vis[k] = v
          })
          return vis
        })
    })
}

ProjectSchema.statics.addNewUpload = function (projectId, pathInProject) {
  const u = new Upload()
  return u.save()
    .then((upload) => {
      return this.findByIdAndUpdate(projectId, { $addToSet: { [pathInProject]: upload } })
        .then(() => {
          return upload
        })
    })
}
ProjectSchema.statics.getFull = function (projectId) {
  return populateProject(this.findById(projectId).select('+user'))
}

ProjectSchema.statics.getFullVoter = function (projectId, userId) {
  return System.findOne({})
    .populate({
      path: 'currentStage',
      options: { lean: true }
    })
    .lean()
    .then((sys) => {
      console.log('sys', sys.currentStage)
      return populateProject(this.findById(projectId).select('+editorPrice +editorTzBuilding +editorTzFlat +editorPlannedStructure +editorEnginiringSystems +peopleFav'), userId, null, true)
        .then((p) => {
          if (!sys.showCustomLikes) {
            delete p.customLikes
          }
          return _.assign(p, { hideLikeButtons: sys.currentStage && sys.currentStage.order > 0 })
        })
    })
}

ProjectSchema.statics.getFullAdmin = function (projectId) {
  return populateProject(this.findById(projectId).select(hiddenFieldsPlus), null, true)
}
ProjectSchema.statics.getFullAndUpdateReadiness = function (projectId, isAdmin) {
  return Promise.resolve()
    .then(() => {
      if (isAdmin) {
        return this.getFullAdmin(projectId)
      } else {
        return this.getFull(projectId)
      }
    })
    .then((obj) => {
      obj.readiness = Math.round((obj.reqs.superDone / obj.reqs.superTotal) * 10000) / 100
      obj.readinessAdmin = countAdminReadiness(obj)
      return this.findByIdAndUpdate(projectId, {
        readiness: obj.readiness,
        readinessAdmin: obj.readinessAdmin
      })
        .then(() => obj)
    })
}

ProjectSchema.statics.removeVis = function (projectId, pathInProject, visId) {
  return this.findByIdAndUpdate(projectId, { $pull: { [pathInProject]: visId } })
    .then((deleted) => {
      return deleted
    })
}

ProjectSchema.statics.findAdmin = function (search) {
  console.log('aa')
  return this.find(search)
    .select(hiddenFieldsPlus)
    // .populate({
    //   path: 'user',
    //   select: '+id4 +id6'
    // })
    // .populate({
    //   path: 'cover',
    //   populate: { path: 'uploads', select: 'url', options: { limit: 1 } }
    // })
    .then(() => {
    return this.aggregate([{
      "$match": {
        isSubmitted: true
      }
    },
    {
      $lookup: {
        from: 'users',
        localField: 'user',
        foreignField: '_id',
        as: 'user'
      }
    }, {
      $unwind: '$user'
    }, {
      $lookup: {
        from: 'vis',
        localField: 'cover',
        foreignField: '_id',
        as: 'cover'
      }
    },
    {
      $unwind: '$cover'
    }, {
      $lookup: {
        from: 'uploads',
        localField: 'cover.uploads',
        foreignField: '_id',
        as: 'cover'
      }
    }, {
      $sort: {'score': -1}
    }, {
      $addFields: {
        cover: { $arrayElemAt: ['$cover', 0] }
      }
    }
    ])
  })
  .then((projects) => {
    var promises = []
     projects.forEach(function (p) {
       // console.log('p.score', p.score)
       // console.log( currentProject.user.viewName)
       // Promise.resolve()
      //  .then(() => {
       // promises.push( new Promise((resolve) => {
          let score = 0
          p.newScore=0
          p.computed = false
          let firstLike = true
         // p.score = 0
         promises.push(   User.find({ role: 'voter' })
        .lean()
        .then((users) => {
         // console.log('user length', users.length)
          var usrPromises = []
          p.newScore=0
          users.forEach(function (user) {
         // console.log('p.id', p._id, 'user._id', user._id)
          //p.score=0
          usrPromises.push(  Like.find({ user: user._id })
           .populate({
            path: 'project',
            select: '_id'
          })
          .lean()
          .then((likes) => {
            //var likePromises = []         
            if (likes.length === 20) {
              
              //var score = 0
              likes.forEach(function (l) {
                //var score = 0
               // console.log('l.project._id', l.project._id, 'p._id 2', p._id)
              //  console.log('p._id === l.project._id', p._id === l.project._id)
                if (p._id.toString() === l.project._id.toString()) {
                  p.newScore = p.newScore + scoreValueMap[l.value]
                //  console.log('newScore', scoreValueMap[l.value])
                 // console.log('p.id', p._id, 'user._id', user._id)
                // if ( p._id.toString() === '5bdb6815e02f5f7c33d0986a') {
                  if ( p._id.toString() === '5bdcf6ced0c28465c7a24272') {
                  console.log('newScore', user._id, user.username, l.value, p.newScore)
              }
                }
              })
              //console.log('>>>>>>>>>>>>>>>>>')
            } else {
              //if ( p._id.toString() === '5bdcf6ced0c28465c7a24272') {
                 console.log('not all likes', user._id,  p.newScore)
                  // likes.forEach(function (l) {
                  //   if ( p._id.toString() === l.project._id.toString()) {
                  //     console.log('not all likes', user._id, l.value, p.score)
                  //   }
                  // })
             // }
            }
           // console.log('likes.length', likes.length)
            //console.log('newScore', score)
             //p.score = p.score + score
             //console.log('new p Score', p.score)
            // console.log('>>>>>>>>>>>>>>>>>')
             return p
          })
          )
        })
        return Promise.all(usrPromises)

        })
     )
        
         //console.log('newScore', score)
        //p.score = score
        // console.log('return resolve(p)')
        //return resolve(p)
    })
       // )
  
  return Promise.all(promises)
  //console.log('return projects')
   // return projects

    
  })
  .then((projectsArray) => {
    console.log('projectsArray')
   // var promises = []
    var projects = []
    for (var i = 0; i< projectsArray.length; i++) {
      let p = projectsArray[i]
      
      console.log('p.length', p.length)
      // for (var j=0; j < p.length; j++) {
      //   //var u = p[j]
      //   console.log('p.id', p[j]._id)
      //   //p[0].score = p[0].score + p[j].score
      //     console.log('u.score', p[j].score)        
      // }
      console.log('u.score', p[0]._id, p[0].newScore)     
      projects.push(p[0])
     this.findByIdAndUpdate(p[0]._id, { 'newScore': p[0].newScore })
     .then(() =>{ return })
    }
    return projects // Promise.all(promises)
  })
  .then((projects) => {
    console.log('end projects')
   return projects.sort(function(a,b) {
       //console.log('a.score', a.score)
       return parseInt(b.newScore) - parseInt(a.newScore)
   })

  })
    
}
function getUserRnd(userId) {
  return Math.abs(Math.sin((parseInt(String(userId).split('').reverse().join(''), 16)))) * 100000
}


ProjectSchema.statics.getNextPrev = function (projectId, userId, isNext) {
  return System.findOne({})
    .lean()
    .then((sys) => {
      return this.findById(projectId)
        //.select('+id4')
        .populate({ path: 'user', select: 'id4 viewName' })
        .select('user.id4 user.viewName')
        .then((currentProject) => {
          console.log(currentProject)
          const userRnd = getUserRnd(userId)
          //const currentRnd = (userRnd + currentProject.rnd) % 9
          //let match // = isNext ? { rndsort: { $gt: currentRnd } } : { rndsort: { $lt: currentRnd } }
          const sort = isNext ? { 'user.viewName': 1 } : { 'user.viewName': -1 }

          // if (sys.orderSort) {
          //   sort = isNext ? { id4: 1 } : { id4: -1 }
          let match = isNext ? { 'user.id4': { $gt: currentProject.user.id4 } } : { 'user.id4': { $lt: currentProject.user.id4 } }
          // }

          // if (sys.showCustomLikes) {
          //   sort = isNext ? { customLikes: -1 } : { customLikes: 1 }
          //   match = isNext ? { customLikes: { $lte: currentProject.customLikes } } : { customLikes: { $gte: currentProject.customLikes } }
          // }
          const currentId = currentProject._id

         // match = _.assign(match, { _id: { $ne: currentProject._id } })
          const proj = {
            'rndsort': 1,
            'id4': '$user.id4',
            'name': '$user.viewName'
          }
          return Promise.resolve()
          .then(() => {
            return this.aggregate([{
              $match: { isSubmitted: true }
            }, {
              $addFields: {
                rndsort: { $mod: [{ $add: [userRnd, '$rnd'] }, 9] }
              }
            }, {
              $lookup: {
                from: 'users',
                localField: 'user',
                foreignField: '_id',
                as: 'user'
              }
            }, {
              $unwind: '$user'
            }, {
              $sort: sort
            }, {
              $skip: 0
            }, {
              $limit: 21
            }, {
              $project: proj
            }])
          })
          .then((projects) => {
            //console.log(projects)
            console.log(currentId)
            let nextPId = null
            projects.forEach(function (p, i, array) {
            //  console.log(p._id, p.name)
             // console.log( currentProject.user.viewName)
              if (p.name === currentProject.user.viewName && i < 20) {
                  //console.log('currentId p', p)
                 // console.log('currentId next', array[ i + 1 ]._id)
                  nextPId = _.pick(array[ i + 1 ], '_id')
              }
            });
            //return (p && p[0]) ? _.pick(p[0], '_id') : null
            return nextPId
          })
        })
        // .then((pid) => {
        //   return pid
        // })
        // .then((projects, currentId) => {
        //   console.log(projects)
        //   console.log(currentId)
        //   projects.forEach(function (p, i) {
        //     if (p._id === currentId) {
        //         console.log(p)
        //         return projects[ i + 1 ]._id
        //     }
        //   });
        //   //return (p && p[0]) ? _.pick(p[0], '_id') : null
        //   return null
        // })
    })
}

ProjectSchema.statics.findVoter = function (search, userId, skip, limit) {
  console.log('find voter')
  const userRnd = getUserRnd(userId)
  const match = {}
  if (search.id4) {
    match['user.id4'] = search.id4
  }
  const proj = {
    'cover.url': 1,
    'rndsort': 1,
    'peopleFav': 1,
    'editorFav': 1,
    'id4': '$user.id4',
    'name': '$user.viewName'
  }
 // let sort = { rndsort: 1 }
  const sort = { 'user.viewName': 1 }
  delete search.id4
  const initialMatch = _.assign(search, { isSubmitted: true })
  return System.findOne({})
    .lean()
    .then((sys) => {
      // if (sys.showCustomLikes) {
      //   proj.customLikes = 1
      //   sort = { customLikes: -1 }
      // }
      if (sys.showFinalists) {
        proj.isFinalist = 1
      }
      // if (sys.orderSort) {
      //   sort = { id4: 1 }
      // }
      if (sys.onlyCurrentsShown) {
        return Stage.getPrevious()
          .then((cs) => {
            console.log('cscs', cs)
            // if (cs && cs.passedWorks && cs.passedWorks.length) {
            //   initialMatch._id = { $in: _.map(cs.passedWorks, 'project') }
            // }
            return null
            // const ids = _.map(cp, (p) => ObjectId(String(p.project._id)))
            // initialMatch._id = { $in: ids }
            // return null
          })
      }

      return null
    })
    .then(() => {
      return this.aggregate([{
        $match: initialMatch
      }, {
        $addFields: {
          rndsort: { $mod: [{ $add: [userRnd, '$rnd'] }, 9] }
        }
      }, {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user'
        }
      }, {
        $unwind: '$user'
      }, {
        $match: match
      }, {
        $sort: sort
      }, {
        $skip: skip
      }, {
        $limit: limit
      }, {
        $lookup: {
          from: 'vis',
          localField: 'cover',
          foreignField: '_id',
          as: 'cover'
        }
      }, {
        $unwind: '$cover'
      }, {
        $lookup: {
          from: 'uploads',
          localField: 'cover.uploads',
          foreignField: '_id',
          as: 'cover'
        }
      }, {
        $addFields: {
          cover: { $arrayElemAt: ['$cover', 0] }
        }
      }, {
        $project: proj
      }])
    })
    .then((projects) => {
      return System.findOne({})
        .select('currentStage')
        .lean()
        .then((system) => {
          return Promise.map(projects, (p) => {
            return Like.findOne({
              stage: system.currentStage,
              project: p._id,
              user: userId
            })
              .lean()
              .then((like) => {
                if (like) {
                  p.like = like.value //  === 1 ? '+' : '-'
                }
                return p
              })
          })
        })
    })
}

ProjectSchema.statics.convertAlbum = function (projectId) {
  return this.findById(projectId)
    .populate({
      path: 'album',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
    .select('+id6')
    .lean()
    .then((project) => {
      if (!project.albumConverted) {
        const v = new Vis({
          project: project._id,
          user: project.user
        })
        return v.save()
          .then((vis) => {
            return this.findByIdAndUpdate(project._id, {
              albumConverted: vis
            })
              .then(() => {
                project.albumConverted = vis
                return project
              })
          })
      }
      return project
    })
    .then((project) => {
      return Vis.findByIdAndUpdate(project.albumConverted, { uploads: [] })
        .then(() => {
          return project
        })
    })
    .then((project) => {
      if (!project.album || !project.album.uploads || project.album.uploads.length <= 0) {
        console.error('no album found', projectId)
        return Promise.resolve()
      }
      return pdf(project.album.uploads[0].url, project.id6)
        .then((results) => {
          console.log('pdf res', results)
          return Promise.each(results.message, (item) => {
            const newPath = item.path.split(path.resolve(config.uploadDir))[1]
            const up = new Upload({
              url: newPath,
              localUrl: newPath,
              state: 'transferred',
              project: projectId,
              keyPath: `albumConverted.${item.page}`
            })
            return up.save()
              .then((upSaved) => {
                console.log('..')
                return Vis.findByIdAndUpdate(project.albumConverted, { $addToSet: { uploads: upSaved } })
              })
          })
        })
    })
}

ProjectSchema.statics.convertAllVis = function (pid) {
  console.log(`try to find prject [${pid}]`)
  return this.findById(pid)
    .select('vis')
    .lean()
    .then((p) => {
      console.log(`vis found [${(p.vis || []).length}]`)
      return Promise.each(p.vis || [], (v) => {
        return Vis.convert(v)
          .then((newVis) => {
            console.log(`vis converted [${pid}] ${newVis._id}`)
            return this.findByIdAndUpdate(pid, { $addToSet: { visConverted: newVis } })
          })
      })
    })
}

ProjectSchema.statics.convertPanel = function (pid, panel, field) {
  console.log('z', pid, panel, field);
  return Promise.resolve()
    .then(() => {
      if (panel) {
        return Vis.convert(panel)
          .then((nv) => {
            console.log(`vis converted [${pid}] ${nv._id}`)
            const obj = { [field]: nv._id }
            console.log(obj)
            return this.findByIdAndUpdate(pid, obj, { new: true })
              .then((proj) => {
                console.log('ppp ', field, proj[field])
              })
          })
      }
      return null
    })
}

ProjectSchema.statics.convertAllPanels = function (pid) {
  console.log(`try to find prject [${pid}]`)
  return this.findById(pid)
    .select('panel1 panel2')
    .lean()
    .then((p) => {
      console.log(`vis found [${(p.vis || []).length}]`)
      return Promise.all([
        this.convertPanel(pid, p.panel1, 'panel1converted'),
        this.convertPanel(pid, p.panel2, 'panel2converted')
      ])
    })
}

function countAdminReadiness(project) {
  return !_.isNil(project.editorPrice) && !_.isNil(project.editorTzFlat) && !_.isNil(project.editorTzBuilding) && !_.isNil(project.editorPlannedStructure)  && !_.isNil(project.editorEnginiringSystems) && (project.cover && project.cover.uploads && project.cover.uploads.length > 0)
}

function removeTemplates(obj) {
  const res = _.cloneDeep(obj)
  delete res.vis
  _.each(res.items, (block) => {
    _.each(block.items, (subBlock) => {
      delete subBlock.vis
    })
  })
  return res
}
function sortAllItems(obj, template) {
  // blocks
  obj.items = obj.items.sort(function (a, b) {
    const ai = _.findIndex(template.items, (o) => String(o.key) === String(a.key))
    const bi = _.findIndex(template.items, (o) => String(o.key) === String(b.key))
    return ai > bi
  })
  // subblocks
  _.each(obj.items, function (block) {
    const tmpl = templates.blocks[block.key];
    block.items = block.items.sort(function (a, b) {
      const ai = _.findIndex(tmpl.items, (o) => String(o.key) === String(a.key))
      const bi = _.findIndex(tmpl.items, (o) => String(o.key) === String(b.key))
      return ai > bi
    })

    // teps
    // _.each(block.items, function (subblock) {
    //   subblock.items = subblock.items.sort(function (a, b) {
    //     const ai = _.findIndex(tmpl.teps, (o) => String(o.key) === String(a.key))
    //     const bi = _.findIndex(tmpl.teps, (o) => String(o.key) === String(b.key))
    //     return ai > bi
    //   })
    // })
  })
  return obj;
  //   let objTemplate = null
  //   if (obj.rnd) {
  //     objTemplate = templates.projects[0]
  //   } else if (obj.mongoKey) {
  //     const numb = obj.mongoKey.split('.')
  //     if (numb.length === 2) {
  //       // block
  //       objTemplate = templates.blocks[obj.key]
  //     } else if (numb.length === 3) {
  //       objTemplate = templates.blocks[obj.key]
  //     } else if (numb.length === 4){
  //       objTemplate = templates.teps[obj.key]
  //     }
  //   } else{
  //     console.log('UNKNOWN obj', obj)
  //   }
  //   // todo detect what it is by model
  //   _.each(obj.items, function (item) {
  //     sortAllItems(item, objTemplate)
  //   })
  // }
}
function fillTepsWithTemplatesData(project) {
  _.each(project.items, (block) => {
    _.each(block.items, (subBlock) => {
      _.each(subBlock.items, (tep) => {
        _.each(templates.teps[tep.key], (v, k) => {
          if (k === 'limits') {
            tep['limits'] = v[subBlock.key]
          } else {
            tep[k] = v
          }
          if (k === 'type' && v === 'range' && !tep.value) {
            tep.value = []
          }
        })
      })
      _.each(subBlock.vis, (vis) => {
        _.each(templates.vis[vis.key], (v, k) => {
          vis[k] = v
        })
      })
      subBlock.name = templates.subBlocks[subBlock.key].name
      subBlock.notes = templates.blocks[block.key].notes
    })

    block.shouldSelected = templates.blocks[block.key].shouldSelected
    block.name = templates.blocks[block.key].name
  })
  _.each(project.vis, (vis) => {
    _.each(templates.vis['baseVis'], (v, k) => {
      vis[k] = v
    })
  })
  _.each(baseVisFields, (vis) => {
    _.each(templates.vis[vis], (v, k) => {
      project[vis][k] = v
    })
  })
}

function populateRootVis(np) {
  let res = np
  _.each(baseVisFields, (field) => {
    res = res.populate({
      path: field,
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
  })
  return res
}

function populateProject(np, userId, isAdmin, isVoter) {
  const res = np.lean()
    .populate({
      path: 'vis',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
    .populate({
      path: 'panel1converted',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
    .populate({
      path: 'panel2converted',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
    .populate({
      path: 'visConverted',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
    .populate({
      path: 'albumConverted',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })
    .populate({
      path: 'user',
      select: isAdmin ? '+id4 +id6' : (isVoter ? '+id4' : '+id6'),
      options: { lean: true }
    })
    .populate({
      path: 'items.items.vis',
      options: { lean: true },
      populate: {
        path: 'uploads',
        options: { lean: true }
      }
    })

  if (isAdmin) {
    res.select('+id6')
  }
  return populateRootVis(res)
    .then(function (p) {
      if (isVoter) {
        return populateLikes(p, userId);
      }
      return p
    })
    .then(function (p) {
      if (p) {
        fillTepsWithTemplatesData(p)
        computeRequires(p)
        setKeys(p)
        if (isAdmin) {
          p.paths = {
            csv: path.resolve(config.uploadDir, `${p.id6}_csv`),
            files: path.resolve(config.uploadDir, `${p.id6}_dl`)
          }
        }
        p = sortAllItems(p, templates.projects[0])
      }
      return p
    })
}

function populateLikes(project, userId) {
  console.log('ppl')
  return System.findOne({})
    .select('currentStage')
    .lean()
    .then((system) => {
      return Like.findOne({
        stage: system.currentStage,
        project: project._id,
        user: userId
      })
        .lean()
    })
    .then((like) => {
      console.log(' LIKE ', like)
      if (like) {
        project.like = like.value// === 1 ? '+' : '-'
      }
      return project
    })
}

function computeRequires(project) {
  let superDone = 0
  let superTotal = 0
  let markableBlocks = 0
  let markedBlocks = 0
  const common = _.reduce(project, (acc, item, key) => {

    switch (key) {
      case 'isApproved':
      case 'isSubmitted':
      case 'readinessAdmin':
      case 'readiness':
      case 'peopleFav':
      case 'editorFav':
      case 'editorPrice':
      case 'editorTzFlat':
      case 'editorTzBuilding':
      case 'editorPlannedStructure':
      case 'editorEnginiringSystems':
      case 'editionAllowedTill':
      case 'user':
      case 'schemas':
      case '_id':
      case 'cover':
      case 'items':
      case 'rnd':
      case 'score':
      case 'vis':
      case 'visConverted':
      case 'csv':
      case 'download':
      case 'id6':
      case 'albumConverted':
      case 'customLikes':
      case 'panel1converted':
      case 'panel2converted':
        break

      case 'desc':
        const v = (((item.ru && item.ru.length > 0) || (item.en && item.en.length > 0)) ? 1 : 0)
        acc.done = acc.done + v
        acc.total += 1
        // superDone += v
        // superTotal += 1
        break
      default:
        const vv = ((item.uploads && item.uploads.length > 0) ? 1 : 0)
        acc.done = acc.done + vv
        acc.total += 1
        // superDone += vv
        // superTotal += 1
        break
    }
    return acc
  }, {
      total: 0,
      done: 0
    })

  const data = _.reduce(project.items, (acc, block, key) => {
    acc.blocks[block._id] = _.reduce(block.items, (accBlock, subBlock, key) => {

      let obj = _.reduce(subBlock.items, (accSubBlock, tep, key) => {
        if (!tep.isRequired && _.indexOf(block.exclude, tep.key)) {
          return accSubBlock
        }
        let v
        if (tep.type === 'range') {
          v = (_.isUndefined(tep.value) || tep.value.length === 0 || tep.value[0] === null || tep.value[1] === null) ? 0 : 1
        } else {
          v = _.isUndefined(tep.value) ? 0 : 1
        }
        // const v = tep.type === 'range' ? (((_.isUndefined(tep.value) || tep.value[0]===null || tep.value[1] === null )? 0 : 1) : (_.isUndefined(tep.value) ? 0 : 1)
        accSubBlock.done += v
        accSubBlock.total += 1
        // superDone += v
        // superTotal += 1
        return accSubBlock
      }, {
          total: 0,
          done: 0
        })

      obj = _.reduce(subBlock.vis, (accSubBlock, vis, key) => {
        if (!vis.isRequired) {
          return accSubBlock
        }
        const v = (vis.uploads && vis.uploads.length > 0) ? 1 : 0
        accSubBlock.done += v
        accSubBlock.total += 1
        // superDone += v
        // superTotal += 1
        subBlock.reqs = accSubBlock
        return accSubBlock
      }, obj)
      if (obj.done >= obj.total && (!block.shouldSelected || subBlock.marked)) {
        accBlock.done += 1
        // superDone += 1
      }

      if (!block.shouldSelected) {
        accBlock.total += 1
        // markableBlocks++
        // superTotal += 1
      } else {
        accBlock.total = block.shouldSelected

        // superTotal += block.shouldSelected
        // superTotal += block.shouldSelected
      }
      accBlock.subs[subBlock._id] = obj
      block.reqs = accBlock
      return accBlock
    }, {
        total: 0,
        done: 0,
        subs: {}
      })
    if (acc.blocks[block._id].done >= acc.blocks[block._id].total) {
      acc.done += 1
      // superDone += 1
    }
    acc.total += 1
    // superTotal += 1
    return acc
  }, {
      total: 0,
      done: 0,
      blocks: {}
    })

  const vis = _.reduce(project.visConverted, (acc, v, key) => {
    acc.done += ((v && v.uploads && v.uploads.length > 0) ? 1 : 0)
    return acc
  }, {
      total: 5,
      done: 0
    })
  vis.done = Math.min(vis.done, 5)
  // superDone += vis.done
  // superTotal += 5
  superTotal = common.total + data.total + vis.total
  superDone = common.done + data.done + vis.done
  project.reqs = { common, data, vis, superDone, superTotal }
  return project.reqs
}

// todo this could be improved

function seedAllInternalObjects(projectId, user, obj) {
  return Promise.all([
    Promise.map(baseVisFields, (u) => ModelClass.addNewVis(projectId, user, u, {}, false)),
    Promise.map(obj.vis, (v) => ModelClass.addNewVis(projectId, user, 'vis', v, true)),
    Promise.each(obj.items, (block, bIndex) => {
      return Promise.each(block.items, (subBlock, sIndex) => {
        return Promise.each(subBlock.vis, (v) => ModelClass.addNewVis(projectId, user, `items.${bIndex}.items.${sIndex}.vis`, v, true))
      })
    })
  ])
}
function setKeys(project) {
  _.each(project.items, (block, bIndex) => {
    block.mongoKey = `items.${bIndex}`
    _.each(block.items, (subBlock, sbIndex) => {
      subBlock.mongoKey = `items.${bIndex}.items.${sbIndex}`
      _.each(subBlock.items, (tep, tIndex) => {
        tep.mongoKey = `items.${bIndex}.items.${sbIndex}.items.${tIndex}`
      })
      _.each(subBlock.vis, (vis, vIndex) => {
        vis.mongoKey = `items.${bIndex}.items.${sbIndex}.vis.${vIndex}`
      })
    })
  })
}


const ModelClass = mongoose.model('Project', ProjectSchema)
module.exports = ModelClass
