'use strict'

var express = require('express')
var auth = require('../../auth/auth.service')
var uploadCtrl = require('../uploads/upload.controller')
var visCtrl = require('../vis/vis.controller')
var projectCtrl = require('./project.controller')
var uploadHelper = require('../../helpers/uploadHelper')

var router = express.Router()

// project

router.get('/', auth.isAuthenticated(), projectCtrl.list)
router.post('/', auth.hasRole('admin'), projectCtrl.create)
router.get('/:id/showNext', auth.isAuthenticated(), projectCtrl.showNext)
router.get('/:id/showPrev', auth.isAuthenticated(), projectCtrl.showPrev)
router.post('/:id/like', auth.hasRole('voter'), projectCtrl.like)
router.get('/:id/like', auth.isAuthenticated(), projectCtrl.getLike)
// router.get('/transferId6', projectCtrl.transferId6)
// router.get('/recalcAllReqs', projectCtrl.recalcAllReqs)
router.get('/:id', auth.isAuthenticated(), projectCtrl.getFull)

router.put('/:id', auth.hasRole('admin'), projectCtrl.update)
router.post('/:id/deadline', auth.hasRole('admin'), projectCtrl.setDeadline)
router.post('/:id/download', auth.hasRole('admin'), projectCtrl.download)
router.post('/:id/assignID4', auth.hasRole('admin'), projectCtrl.assignID4)
router.post('/:id/csv', auth.hasRole('admin'), projectCtrl.csv)

// clients adding vis
router.post('/:projectId/vis', auth.isAuthenticated(), projectCtrl.addNewVis)
router.delete('/:projectId/vis/:visId', auth.isAuthenticated(), projectCtrl.removeVis)
router.put('/:projectId/vis/:visId', auth.isAuthenticated(), projectCtrl.updateVis)

module.exports = router
