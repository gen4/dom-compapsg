'use strict'

var _ = require('lodash')
var Promise = require('bluebird')
var fs = require('fs')
var path = require('path')
var Project = require('./project.model')
var Vis = require('../vis/vis.model')
var User = require('../user/user.model')
var Like = require('../like/like.model')
var System = require('../system/system.model')
var CommonRestHelper = require('../../helpers/restHelper')
var config = require('../../../config').backend
var handleErrorAsync = require('../errorhandler')
var request = require('request-promise')

const rest = new CommonRestHelper(Project)

const scoreValueMap = {
  0: 0,
  1: 8,
  2: 3,
  3: 1
}


const update = {
  create: function (req, res, next) {
    Project.createEmptyWithUser(req.body.user)
      .then((np) => {
        return res.json(np)
      })
      .catch(handleErrorAsync(res))
  },
  addNewVis: function (req, res, next) {
    const { projectId } = req.params
    Project.addNewVis(projectId, req.user, 'visConverted', req.body, true)
      .then(function (saved) {
        return res.json(saved)
      })
      .catch(handleErrorAsync(res))
  },
  removeVis: function (req, res, next) {
    const { projectId, visId } = req.params;
    Project.removeVis(projectId, 'visConverted', visId)
      .then(function (removed) {
        return res.json(removed)
      })
      .catch(handleErrorAsync(res))
  },
  updateVis: function (req, res, next) {
    const { projectId, visId } = req.params
    if (!req.body || !req.body.name) {
      return res.status(400).send(new Error('no vis-name sent'))
    }
    const obj = {
      'name.ru': req.body.name.ru,
      'name.en': req.body.name.en
    }
    console.log('obj', visId, obj)
    return Vis.findByIdAndUpdate(visId, obj, { new: true })
      .then((vis) => {
        console.log('r', vis)
        return res.json(vis)
      })
      .catch(handleErrorAsync(res))
  },
  getFull: function (req, res, next) {
    const { id } = req.params

    Promise.resolve()
      .then(() => {
        console.log('get full ', req.user)
        if (req.user && req.user.role === 'admin') {
          return Project.getFullAdmin(id)
        } else if (req.user && req.user.role === 'voter') {
          return Project.getFullVoter(id, req.user._id)
        } else {
          return Project.getFull(id)
        }
      })
      .then(function (p) {
        return res.json(p)
      })
      .catch(handleErrorAsync(res))
  },
  update: function (req, res, next) {
    return Project.findByIdAndUpdate(req.params.id, req.body, { new: true })
      .then((p) => {
        return Project.getFullAndUpdateReadiness(p._id, req.user.role === 'admin')
      })
      .then((p) => {
        return res.json(p)
      })
      .catch(handleErrorAsync(res))
  },
  setDeadline: function (req, res, next) {
    return Project.findById(req.params.id)
      .select('editionAllowedTill')
      .lean()
      .then((p) => {
        let newDate
        if (req.body.minutesPlus > 0) {
          newDate = new Date(p.editionAllowedTill).getTime() + req.body.minutesPlus * 60 * 1000
        } else {
          newDate = new Date().getTime() - 1000
        }
        return Project.findByIdAndUpdate(req.params.id, { editionAllowedTill: newDate })
      })
      .then((p) => {
        return res.json(p)
      })
      .catch(handleErrorAsync(res))
  },
  list: function (req, res, next) {
    const count = +req.query.count || 20
    const page = +req.query.page || 0
    const search = req.query.search || {}
    if (search.id6 && search.id6.length > 0) {
      search['$or'] = [{ id6: { $regex: `^${String(search.id6)}.*` } }, { id4: { $regex: `^${String(search.id6)}.*` } }]
      delete search['id6']
    } else {
      delete search['id6']
    }
    // if (req.user.role === 'voter' || req.user.role ===) {
    if (search.id4 && search.id4.length > 0) {
      search['id4'] = { $regex: `^${String(search.id4)}.*` }
    } else {
      delete search['id4']
    }
    // } else {
    //   delete search.id4
    // }


    const skip = +count * +page
    const readinessFilter = req.query.readinessFilter

    if (readinessFilter === '50') {
      search.readiness = { $gte: 50 }
    } else if (readinessFilter === 'started') {
      search.readiness = { $gt: 0 }
    } else if (readinessFilter === 'submitted') {
      search.isSubmitted = true
    } else {

    }
    const limit = +count
    console.log('req.user', req.user)
    console.log('search ', JSON.stringify(search))
    Promise.all([
      findProject(search, req.user.role === 'admin', !req.query.isAdmin && (req.user.role === 'admin' || req.user.role === 'voter' || req.user.role === 'user'), req.user._id, skip, limit),
      Project.count(search)
    ])
      .spread(function (results, total) {
        res.json({ results: results, total: total })
      })
      .catch(handleErrorAsync(res))
  },
  download: function (req, res, next) {
    const pid = req.params.id;
    request({
      method: 'POST',
      url: `http://localhost:${config.downloaderPort}/startDownload/${pid}`
    })
      .then((p) => {
        return res.json(p)
      })
      .catch(handleErrorAsync(res))
  },
  csv: function (req, res, next) {
    const pid = req.params.id;
    request({
      method: 'POST',
      url: `http://localhost:${config.downloaderPort}/csv/${pid}`
    })
      .then((p) => {
        Project.findByIdAndUpdate(pid, { csv: true })
          .then(() => {
            return res.json(p)
          })
      })
      .catch(handleErrorAsync(res))
  },
  assignID4: function (req, res, next) {
    const pid = req.params.id
    Project.findById(pid)
      .select('user')
      .then((p) => {
        return User.assignID4(p.user)
          .then(() => {
            return User.findById(p.user)
              .select('+id4')
              .then((user) => {
                return Project.findByIdAndUpdate(pid, { id4: user.id4 })
              })
          })
      })
      .then(() => {
        return res.json({})
      })
      .catch(handleErrorAsync(res))
  },
  showNext: function (req, res) {
    const { id } = req.params
    console.log(id)
    return Project.getNextPrev(id, req.user._id, true)
      .then((project) => {
        return res.json(project)
      })
      .catch(handleErrorAsync(res))
  },
  showPrev: function (req, res) {
    const { id } = req.params
    return Project.getNextPrev(id, req.user._id, false)
      .then((project) => {
        return res.json(project)
      })
      .catch(handleErrorAsync(res))
  },
  like: function (req, res) {
    console.log('like')
    return System.findOne()
      .select('currentStage')
      .populate('currentStage')
      .lean()
      .then((system) => {
        console.log('like2')
        if (system.currentStage.order > 0 || (system.currentStage.dueDate && (new Date(system.currentStage.dueDate).getTime() < new Date().getTime()))) {
          console.log('Like is not allowed')
          return { disabled: true }
        }
        console.log('like3')
        return Like.findOne({
          stage: system.currentStage._id,
          project: req.params.id,
          user: req.user._id
        })
          .lean()
          .then((like) => {
            console.log('like4', like)
            // var score = req.body.value === '+' ? 1 : 0
            var score = req.body.value
            if (!like) {
              const obj = {
                stage: String(system.currentStage._id),
                project: req.params.id,
                user: String(req.user._id),
                value: score
              }
              console.log('scoreValueMap new like', scoreValueMap[score])
              Project.findById(req.params.id)
                .select('score')
                .lean()
                .then((p) => {
                  var prevProjScore = p.score
                  if (isNaN(p.score)) {
                    prevProjScore = 0
                  }
                  var projectScore = prevProjScore + scoreValueMap[req.body.value]
                  console.log('scoreValueMap', scoreValueMap[req.body.value])
                  console.log('projectScore', projectScore)
                  return Project.findByIdAndUpdate(req.params.id, { score: projectScore })
                })
              console.log('ob', obj)
              return new Promise((resolve, reject) => {
                return Like.create(obj, function (err, res) {
                  console.log('ob2', err, res)
                  if (err) {
                    return reject(err)
                  }
                  return resolve(res)
                })
              })

              // const l = new Like(obj)
              // console.log('ob2', l)
              // return l.save()
              // .then((s) => {
              //   console.log('ee',s)
              //   return s;
              // })
            } else {
              Project.findById(req.params.id)
                .select('score')
                .lean()
                .then((p) => {
                  var prevProjScore = p.score
                  if (isNaN(p.score)) {
                    prevProjScore = 0
                  }
                  var projectScore = prevProjScore - scoreValueMap[like.value] + scoreValueMap[req.body.value]
                  console.log('scoreValueMap', scoreValueMap[req.body.value])
                  console.log('projectScore', projectScore)
                  return Project.findByIdAndUpdate(req.params.id, { score: projectScore })
                })
              return Like.findByIdAndUpdate(like._id, {
                value: score
              }, { new: true })
            }
          })
      })
      .then((results) => {
        console.log('like5')
        return res.status(200).json({ results: results })
      })
      .catch(handleErrorAsync)
  },

  getLike: function (req, res) {
    console.log('get like')
    return Like.findOne({
      project: req.params.id,
      user: req.user._id
    })
    .select('value')
      .lean()
      .then((like) => {
        console.log('get like like', like)
        if (!like) {
          return 0
        } else {
          return like.value
        }
      })
      .then((value) => {
        console.log('like5')
        return Project.findById(req.params.id)
        .populate({ path: 'user', select: 'viewName' })
        .select('user.viewName')
         .lean()
         .then((project) => {
           console.log('like5 project', project)
           return { like: value, viewName: project.user.viewName }
          })         
      })
      .then((data) => {
        console.log('like5 data', data)
         return res.status(200).json({ like: data.like, name: data.viewName })
      })
      .catch(handleErrorAsync)
  },

  // transferId6: function (req, res, next) {
  //   return Project.find({})
  //     .lean()
  //     .populate({
  //       path: 'user',
  //       select: '+id4 +id6',
  //       options: { lean: true }
  //     })
  //     .then((projects) => {
  //       return Promise.each(projects, (p) => {
  //         return Project.findByIdAndUpdate(p._id, { id6: p.user.id6 })
  //       })
  //     })
  //     .then(() => {
  //       return res.json({})
  //     })
  //     .catch(handleErrorAsync(res))
  // },
  // recalcAllReqs: function (req, res, next) {
  //   return Project.find({})
  //     .lean()
  //     .then((projects) => {
  //       return Promise.each(projects, (p) => {
  //         return Project.getFullAndUpdateReadiness(p._id)
  //       })
  //     })
  //     .then(() => {
  //       return res.json({})
  //     })
  //     .catch(handleErrorAsync(res))
  // }
}

function findProject(search, isAdmin, isVoter, userId, skip, limit) {
  console.log('isVoter', isVoter)
  if (isVoter) {
    return Project.findVoter(_.assign(search, { isSubmitted: true }), userId, skip, limit)
  } else if (isAdmin) {
    return Project.findAdmin(search)


  }
  return Project.find(search).populate({ path: 'cover', populate: { path: 'uploads' } })
    .skip(skip)
    .limit(limit)
    .lean()
}

module.exports = _.assign(rest, update);
