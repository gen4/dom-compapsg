'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var SystemSchema = new Schema({
  currentStage: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Stage'
  },
  showFinalists: { type: Boolean, default: false },
  showCustomLikes: { type: Boolean, default: false },
  orderSort: { type: Boolean, default: false },
  panic: { type: Boolean, default: false },
  onlyCurrentsShown: { type: Boolean, default: true },
  deadline: { type: Date, default: new Date('2018-11-05T12:00:00') },
  votingIsAllowed: { type: Boolean, default: false }
})

module.exports = mongoose.model('System', SystemSchema)
