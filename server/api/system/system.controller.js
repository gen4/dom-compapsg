/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var System = require('./system.model')
var Socket = require('./system.socket')
var handleError = require('../errorhandler')
var fs = require('fs');
var config = require('../../../config').backend
var request = require('request-promise')
var handleErrorAsync = require('../errorhandler')
// Get list of sids
exports.index = function (req, res) {
  System.findOne({})
    .lean()
    .select('-_id -__v')
    .populate({
      path: 'currentStage',
      lean: true
    })
    .then(function (system) {
      return res.status(200).json({ results: system })
    })
    .catch(handleError)
}

exports.update = function(req, res) {
  System.findOneAndUpdate({}, req.body)
    .then(function () {
      return res.status(200).json({ })
    })
    .catch(handleError)
}

exports.switchCurrents = function (req, res) {
  System.findOneAndUpdate({}, { onlyCurrentsShown: req.body.currents }, { new: true })
    .then(function (system) {
      return res.status(200).json({ results: system })
    })
}

exports.setCurrentStage = function (req, res) {
  if (!req.body.stageId) {
    return res.status(404).send()
  } else {
    System.findOneAndUpdate({}, { currentStage: req.body.stageId })
      .then(function (system) {
        // socket.emit
        Socket.stageChanged(system)
        return res.status(200).json({ results: system })
      })
  }
}


exports.setNextStage = function (req, res) {

}

exports.allProjectsCsv = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/allProjectsCsv`,
    json: true
  })
    .then((p) => {
      return res.download(p.filename, 'allProjects.csv')
    })
    .catch(handleErrorAsync(res))
}

exports.totalFetch = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/totalFetch`,
    json: true
  })
    .then((p) => {
      return res.download(p.filename, 'allProjects.csv')
    })
    .catch(handleErrorAsync(res))
}

exports.totalConverts = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/totalConverts`,
    json: true
  })
    .then((p) => {
      return res.status(200).json({})
    })
    .catch(handleErrorAsync(res))
}

exports.totalConvertsPers = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/totalConverts/${req.params.projectId}`,
    json: true
  })
    .then((p) => {
      return res.status(200).json({})
    })
    .catch(handleErrorAsync(res))
}

exports.picfix = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/picfix/${req.params.projectId}`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.picfixAll = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/picfix`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.id4transfer = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/id4transfer`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.setPrices = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/setPrices`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}
exports.genAllCSVs = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/genAllCSVs`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.findMutants = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/findMutants`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.id4transfer = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/id4transfer`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}
exports.setComplience = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/setComplience`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.convertFewAlbums = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/convertFewAlbums`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.allProjectsLikesCsv = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/allProjectsLikesCsv/${req.params.stageId}`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.totalPanelsConverts = function (req, res) {
  request({
    method: 'POST',
    url: `http://localhost:${config.downloaderPort}/totalPanelsConverts`,
    json: true
  })
    .then((p) => {
      return res.status(200).json(p)
    })
    .catch(handleErrorAsync(res))
}

