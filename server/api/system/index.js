'use strict'

var express = require('express')
var controller = require('./system.controller')
var auth = require('../../auth/auth.service')
var router = express.Router()

router.get('/', controller.index)
router.put('/', auth.hasRole('admin'), controller.update)
router.post('/currentStage', auth.hasRole('admin'), controller.setCurrentStage)
router.get('/allProjectsCsv', controller.allProjectsCsv)
router.post('/switchCurrents', controller.switchCurrents)
router.get('/id4transfer', controller.id4transfer)
router.get('/totalConverts/:projectId', controller.totalConvertsPers)
router.get('/setComplience', controller.setComplience)
router.get('/convertFewAlbums', controller.convertFewAlbums)
router.get('/allProjectsLikesCsv/:stageId', controller.allProjectsLikesCsv)
router.get('/totalPanelsConverts', controller.totalPanelsConverts)


// router.get('/totalConverts', controller.totalConverts)
// router.get('/findMutants', controller.findMutants)
// router.get('/picfix/:projectId', controller.picfix)
// router.get('/picfix', controller.picfixAll)
// router.get('/id4transfer', controller.id4transfer)
router.get('/genAllCSVs', controller.genAllCSVs)
// router.get('/setPrices', controller.setPrices)



// router.put('/:id', controller.update)
// router.patch('/:id', controller.update)
// router.delete('/:id', controller.destroy)

module.exports = router
