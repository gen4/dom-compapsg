'use strict'

var mongoose = require('mongoose')
var _ = require('lodash')
var Schema = mongoose.Schema
var crypto = require('crypto')
var jwt = require('jsonwebtoken')
var config = require('../../../config').backend

var UserSchema = new Schema({
  name: String,
  id4: { type: String, select: false, index: true },
  id6: { type: String, select: false, index: true },
  __v: { type: Number, select: false },
  username: { type: String, lowercase: true },
  viewName: { type: String, uppercase: true },
  lang: { type: String },
  email: { type: String },
  role: {
    type: String,
    default: 'user',
    index: true
  },
  hashedPassword: String,
  provider: String,
  salt: String,
  firstCount: { type: Number, default: 5 },
  secondCount: { type: Number, default: 5 },
  thirdCount: { type: Number, default: 10 }
})

/**
 * Virtuals
 */
UserSchema
  .virtual('password')
  .set(function (password) {
    this._password = password
    this.salt = this.makeSalt()
    this.hashedPassword = this.encryptPassword(password)
  })
  .get(function () {
    return this._password
  })

// Public profile information
UserSchema
  .virtual('profile')
  .get(function () {
    return {
      'name': this.name,
      'role': this.role
    }
  })

// Non-sensitive info we'll be putting in the token
UserSchema
  .virtual('token')
  .get(function () {
    return {
      '_id': this._id,
      'role': this.role
    }
  })

/**
 * Validations
 */

// Validate empty username
// UserSchema
//   .path('username')
//   .validate(function (username) {
//     return username.length
//   }, 'Username cannot be blank')

// Validate empty password
// UserSchema
//   .path('hashedPassword')
//   .validate(function (hashedPassword) {
//     return hashedPassword.length
//   }, 'Password cannot be blank')

// Validate username is not taken
// UserSchema
//   .path('email')
//   .validate(function (value, respond) {
//     var self = this
//     this.constructor.findOne({ email: value }, function (err, user) {
//       if (err) throw err
//       if (user) {
//         // if (this.provider === 'remote') {
//         //   return respond(true)
//         // }
//         if (self.id === user.id) return respond(true)
//         return respond(false)
//       }
//       respond(true)
//     })
//   }, 'The specified username is already in use.')

var validatePresenceOf = function (value) {
  return value && value.length
}

/**
 * Pre-save hook
 */
UserSchema
  .pre('save', function (next) {
    if (!this.isNew || this.provider === 'remote') return next()

    if (!validatePresenceOf(this.hashedPassword)) {
      next(new Error('Invalid password'))
    } else {
      next()
    }
  })

/**
 * Methods
 */
UserSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashedPassword
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */
  makeSalt: function () {
    return crypto.randomBytes(16).toString('base64')
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */
  encryptPassword: function (password) {
    if (!password || !this.salt) return ''
    var salt = new Buffer(this.salt, 'base64')
    return crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('base64')
  }
}

UserSchema.statics = {
  getOrCreateNew: function (obj) {
    console.log('Create new user', obj)
    return this.findOne({ id6: obj.id6 })
      .lean()
      .then((existingUser) => {
        if (existingUser) {
          return existingUser
        }
        // return getUniqueID4()
        //   .then((id4) => {
        const newUser = _.assign({}, obj)
        newUser.provider = 'remote'
        newUser.role = 'user'
        // newUser.id4 = id4
        return new this(newUser).save()
          // })
      })
      .then((user) => {
        const token = jwt.sign({ _id: user._id, name: user.name, role: user.role }, config.secrets.session, { expiresIn: '7d' })
        return token
      })
  },
  assignID4 (userId) {
    return this.findById(userId)
      .select('+id4')
      .lean()
      .then((user) => {
        if (!user) {
          return Promise.reject(new Error('wrong user'))
        }
        if (!user.id4) {
          return getUniqueID4()
            .then((id4) => {
              return this.findByIdAndUpdate(userId, { id4 }, { new: true })
            })
        }
        return null
      })
  }
}
function getUniqueID4 () {
  const id4 = getRandomID4()
  return Model.findOne({ id4 })
    .lean()
    .then((existingUser) => {
      if (existingUser) {
        return getUniqueID4()
      }
      return id4
    })
}


function getRandomID4 () {
  return (1000 + Math.floor(Math.random() * 8999)) + ''
}


const Model = mongoose.model('User', UserSchema);
module.exports = Model
