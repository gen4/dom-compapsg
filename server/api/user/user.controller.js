'use strict'

var User = require('./user.model')
var System = require('../system/system.model')
var Like = require('../like/like.model')
var Upload = require('../uploads/upload.model')
var Project = require('../project/project.model')
var handleErrorAsync = require('../../api/errorhandler')
var remoteUser = require('../../helpers/remoteUser')
// var passport = require('passport')
var config = require('../../../config').backend
var jwt = require('jsonwebtoken')
var paging = require('../paging')
var _ = require('lodash')
var Promise = require('bluebird')

var validationError = function (res, err) {
  return res.status(422).json(err)
}

/**
 * Get list of users
 * restriction: 'admin'
 */
exports.index = function (req, res) {
  var search = _.merge(req.query.search)
  if (search.email && search.email.length > 0) {
    search.email = { $regex: `${String(search.email)}.*` }
  } else if (req.query.emailOrId6) {
    search = { $or: [{ email: { $regex: `${String(req.query.emailOrId6)}.*` } }, { id6: { $regex: `^${String(req.query.emailOrId6)}.*` } }, { id4: { $regex: `^${String(req.query.emailOrId6)}.*` } }] }
  } else {
    delete search['email']
  }
  console.log('search', search)
  paging.listQuery(User, search, '-salt -hashedPassword +id6', {}, req.query.page, function (err, json) {
    if (err) return res.status(500).send(err)
    res.status(200).json(json)
  })
}

/**
 * Creates a new user
 */
exports.create = function (req, res, next) {
  console.log(req.body.role)
  if (!req.body.role || req.body.role === 'admin') {
    req.body.role = 'guest'
  }
  req.body.provider = 'local'
  if (req.body.role === 'user') {
    req.body.password = 'userpass'
  }
  Promise.resolve()
    .then(() => {
      console.log(req.body.role)
      if (req.body.role === 'user') {
        return User.findOne({ id6: req.body.id6 }).select('id6').lean()
      } else {
        return User.findOne({ username: req.body.username }).lean()
      }
    })
    .then((existedUser) => {
      console.log('existedUser', req.body.id6, existedUser)
      if (existedUser) {
        return Promise.reject(new Error('User already exists'))
      }
      return null
    })
    .then(() => {
      var newUser = new User(req.body)
      return newUser.save(function (err, user) {
        if (err) return validationError(res, err)
        var token = jwt.sign({ _id: user._id, name: user.name, role: user.role }, config.secrets.session, { expiresIn: '127d' })
        return res.json({ token: token })
      })
    })
    .catch(handleErrorAsync(res))
}

/**
 * Get a single user
 */
exports.show = function (req, res, next) {
  var userId = req.params.id

  User.findById(userId, function (err, user) {
    if (err) return next(err)
    if (!user) return res.sendStatus(404)
    res.json(user.profile)
  })
    .catch(handleErrorAsync(res))
}

/**
 * Deletes a user
 * restriction: 'admin'
 */
exports.destroy = function (req, res) {
  User.findByIdAndRemove(req.params.id, function (err, user) {
    if (err) return res.status(500).send(err)
    return res.sendStatus(204)
  })
    .catch(handleErrorAsync(res))
}

/**
 * Change a users password
 */
exports.changePassword = function (req, res, next) {
  var userId = req.params.id
  console.log('userId', userId)
  var oldPass = String(req.body.oldPassword)
  var newPass = String(req.body.newPassword)

  User.findById(userId, function (err, user) {
    if (err) {
      return res.status(401).json(err)
    }
    if (user.authenticate(oldPass)) {
      user.password = newPass
      user.save(function (err) {
        if (err) return validationError(res, err)
        res.sendStatus(200)
      })
    } else {
      res.status(403).json({ message: 'Old password is not correct.' })
    }
  })
    .catch(handleErrorAsync(res))
}

/**
 * Change a users token count
 */
exports.changeTokenCount = function (req, res, next) {
  var userId = req.user._id
  var firstCount = req.body.firstCount
  var secondCount = req.body.secondCount
  var thirdCount = req.body.thirdCount
  console.log("changeTokenCount", userId, firstCount, secondCount, thirdCount)
  User.findById(userId, function (err, user) {
    if (err) {
      // handler error
    }
    user.firstCount = firstCount
    user.secondCount = secondCount
    user.thirdCount = thirdCount
    user.save(function (err) {
      if (err) return validationError(res, err)
      res.sendStatus(200)
    })

  })
    .catch(handleErrorAsync(res))
}

/**
 * Get my info
 */
exports.me = function (req, res, next) {
  var userId = req.user._id
  User.findOne({
    _id: userId
  }, '-salt -hashedPassword', function (err, user) { // don't ever give out the password or salt
    if (err) return next(err)
    if (!user) return res.json(401)
    res.json(user)
  })
    .catch(handleErrorAsync(res))
}

exports.performance = function (req, res, next) {
  return Promise.all([
    System
      .findOne({})
      .select('currentStage')
      .lean()
      .then((system) => {
        return Like.count({
          user: req.user._id,
          stage: system.currentStage
        })
      }),
    Project.count({ isSubmitted: true })
  ])
    .spread((likesCount, thingsCount) => {
      res.json({ likesCount, thingsCount })
    })
    .catch(handleErrorAsync(res))
}

exports.getProject = function (req, res, next) {
  const returnAlwaysNewProject = false
  if (returnAlwaysNewProject) {
    return Project.createEmptyWithUser(req.user)
      .then((np) => {
        return res.json(np)
      })
      .catch(handleErrorAsync(res))
  }
  return Project.getByUserId(req.user._id)
    .then((project) => {
      if (project) {
        return project
      }
      return Project.createEmptyWithUser(req.user)
    })
    .then((np) => {
      return res.json(np)
    })
    .catch(handleErrorAsync(res))
}

exports.saveProject = function (req, res, next) {
  const project = req.body;
  Project.findOneAndUpdate({ user: req.user._id }, project)
    .then((np) => {
      return res.json(np)
    })
    .catch(handleErrorAsync(res))
}

exports.updateProject = function (req, res, next) {
  const objectToSave = _.omit(req.body, [
    '_id',
    '__v',
    'user',
    'isSubmitted',
    'isApproved',
    'editionAllowedTill',
    'peopleFav',
    'editorFav',
    'editorPrice',
    'editorTzFlat',
    'editorTzBuilding',
    'editorPlannedStructure',
    'editorEnginiringSystems',
    'readiness',
    'readinessAdmin'])
  return Project.findOneAndUpdate({ user: req.user._id }, objectToSave)
    .then((p) => {
      return Project.getFullAndUpdateReadiness(p._id)
    })
    .then((p) => {
      return res.json(p)
    })
    .catch(handleErrorAsync(res))
}

exports.submit = function (req, res, next) {
  // const now = new Date().getTime()
  Project.findOne({ user: req.user._id, isSubmitted: false })
    .lean()
    .select('_id')
    .then((p) => {
      if (!p) {
        return res.status(400).send({ msg: 'Project has been submitted already or not found' })
      }
      return Project.getFullAndUpdateReadiness(p._id)
        .then((p) => {
          if (p.reqs.superTotal > p.reqs.superDone) {
            return res.status(400).send({ msg: 'Project not fully filled yet' })
          }
          return Project.findOneAndUpdate({ user: req.user._id, isSubmitted: false }, {
            isSubmitted: true
          })
            .then(() => {
              return User.assignID4(req.user._id)
              // return User.findById(req.user._id)
              //   .then((user) => {
              //     if (!user) {
              //       return Promise.reject(new Error('wrong user'))
              //     }
              //     if (!user.id4) {
              //       return getUniqueID4()
              //         .then((id4) => {
              //           return User.findByIdAndUpdate(req.user._id, { id4 }).exec()
              //         })
              //     }
              //     return null
              //   })
            })
            .then(() => {
              return res.json({ msg: 'project submitted' })
            })
          // })
        })
    })
    .catch(handleErrorAsync(res))
}

exports.uploadProjectFile = function (fieldName) {
  return function (req, res, next) {
    if (req.files && req.files[0] && req.files[0].filename) {
      return Promise.map(req.files, (file) => {
        const ul = new Upload({
          url: file.filename
        })
        return ul.save()
      })
        .then(function (uls) {
          return Project.findOneAndUpdate({ user: req.user._id }, { $addToSet: { [fieldName]: { $each: uls } } })
            .then(function () {
              return res.json(uls)
            })
        })
        .catch(handleErrorAsync(res))
    } else {
      return res.status(401).json({ error: 'no filename' })
    }
  }
}

// middlewares

exports.isAllowedToEdit = function (req, res, next) {
  console.log('req.user', req.user)
  if (req.user.role === 'admin') {
    return next()
  }
  return Project.findOne({ user: req.user._id })
    .select('editionAllowedTill')
    .lean()
    .then((p) => {
      const now = new Date().getTime()
      const deadline = new Date(p.editionAllowedTill).getTime()
      if (deadline > now) {
        return next()
      }
      return res.status(405).send('Project couldn\'t be edited')
    })
}

exports.isDeadlineMissed = function (req, res, next) {
  console.log('IDLM role', req.user.role)
  if (req.user.role === 'admin') {
    return next()
  }
  Promise.all([
    System.findOne({})
      .select('deadline')
      .lean(),
    Project.findOne({ user: req.user._id })
      .select('editionAllowedTill isSubmitted')
      .lean()
  ])
    .spread((sys, proj) => {
      if (!proj || !proj.editionAllowedTill) {
        // return res.status(403).send('Project couldn\'t be created')
        return next()
      } else if (proj.isSubmitted) {
        return res.status(405).send('Project already submitted')
      } else if (new Date(sys.deadline).getTime() > new Date().getTime() || new Date(proj.editionAllowedTill).getTime() > new Date().getTime()) {
        return next()
      }
      return res.status(403).send('Project couldn\'t be served at now')
    })
  // .then((s) => {
  //   const now = new Date().getTime()
  //   const deadline = new Date(s.deadline).getTime()
  //   if (deadline > now) {
  //     return next()
  //   }
  //   return res.status(405).send('Project couldn\'t be created')
  // })
}


