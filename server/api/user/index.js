'use strict'

var express = require('express')
var controller = require('./user.controller')
var visCtrl = require('../vis/vis.controller')
// var config = require('../../../config').backend
var auth = require('../../auth/auth.service')
var uploadHelper = require('../../helpers/uploadHelper')
var router = express.Router()



router.get('/me', auth.isAuthenticated(), controller.me)
router.get('/me/performance', auth.isAuthenticated(), controller.performance)
router.get('/me/project', auth.isAuthenticated(), controller.isDeadlineMissed, controller.getProject)
router.put('/me/project', auth.isAuthenticated(), controller.isDeadlineMissed, controller.updateProject)
router.post('/me/project/submit', auth.isAuthenticated(), controller.submit)
router.post('/me/token-count', auth.isAuthenticated(), controller.changeTokenCount)

router.get('/', auth.hasRole('admin'), controller.index)
router.post('/', auth.hasRole('admin'), controller.create)
router.delete('/:id', auth.hasRole('admin'), controller.destroy)
router.put('/:id/password', auth.hasRole('admin'), controller.changePassword)
router.get('/:id', auth.hasRole('admin'), controller.show)
module.exports = router
