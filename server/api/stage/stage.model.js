'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema
var System = require('../system/system.model')

var winnerSchema = new Schema({
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Project'
  },
  score: { type: Number },
  likes: { type: Number }
}, {_id: false})
var StageSchema = new Schema({
  dueDate: { type: Date },
  order: { type: Number },
  passedWorks: { type: [winnerSchema], default: [] }
})

StageSchema.statics.getCurrent = function () {
  return System
    .findOne({})
    .lean()
    .then((system) => {
      return this
        .findById(system.currentStage)
        .lean()
    })
}
StageSchema.statics.getPrevious = function () {
  return System
    .findOne({})
    .populate({
      path: 'currentStage',
      select: 'order',
      options: { lean: true }
    })
    .lean()
    .then((system) => {
      return this
        .findOne({ order: { $lt: system.currentStage.order }})
        .sort('-order')
        .lean()
    })
}
module.exports = mongoose.model('Stage', StageSchema)
