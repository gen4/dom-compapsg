/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /sids              ->  index
 */

'use strict'

var _ = require('lodash')
var Project = require('../project/project.model')
var Stage = require('./stage.model')
var System = require('../system/system.model')
var Like = require('../like/like.model')

var handleError = require('../errorhandler')
var Promise = require('bluebird')
// Get list of sids
exports.index = function (req, res) {
  Stage.find({})
    .lean()
    .then(function (stages) {
      return _.map(stages, function (stage) {
        return stage
      })
    })
    .then(function (stages) {
      return res.status(200).json({ results: stages })
    })
    .catch(handleError)
}

exports.create = function (req, res) {
  Stage
    .findOne({})
    .sort('-order')  // give me the max
    .lean()
    .then(function (maxStage) {
      req.body.order = maxStage.order + 1
      return Stage.create(req.body)
    })
    .then(function (stage) {
      return res.status(200).json(stage)
    })
    .catch(handleError)
}

// Updates an existing thing in the DB.
exports.update = function (req, res) {
  if (req.body._id) { delete req.body._id }
  Stage.findById(req.params.id)
    .then(function (stage) {
      if (!stage) { return res.status(404).send() }
      var updated = _.merge(stage, req.body)
      return updated.save({ runValidators: true, context: 'query' })
    })
    .then(function (saved) {
      res.status(200).json(saved)
    })
    .catch(handleError)
}

exports.currentProjects = currentProjects;
exports.getCurrentProjects = function (req, res) {
  return currentProjects()
    .then((projects) => {
      res.status(200).json({ results: projects })
    })
    .catch(handleError)
}

exports.getTop100 = function (req, res) {
  top100()
    .then((projects) => {
      console.log('TE')
      res.status(200).json({ results: projects })
    })
    .catch(handleError)
}
exports.setAsCurrent = function (req, res) {
  if (!req.body.stage) {
    return res.status(401).send()
  }
  console.log('req.body.withoutErease', req.body.withoutErease)
  return Promise.all([
    System.findOne({}),
    Stage.findById(req.body.stage)
  ])
  .spread((system, stage) => {
    if (!stage) {
      return res.status(401).send()
    }
    if (req.body.withoutErease) {
      return Promise.all([
        // Stage.update({ order: { $gt: stage.order }}, { passedWorks: [] }),
        System.findOneAndUpdate({}, { currentStage: req.body.stage })
      ])
        .then(() => {
          res.status(200).json({ results: [] })
        })
    } else {
      return Promise.all([
        Stage.update({ order: { $gt: stage.order }}, { passedWorks: [] }),
        System.findOneAndUpdate({}, { currentStage: req.body.stage })
      ])
        .then(() => {
          res.status(200).json({ results: [] })
        })
    }
  })
}

exports.finalizeCurrent = function (req, res) {
  if (!req.body.winners || req.body.winners.length <= 0) {
    console.error('no body', req.body)
    return res.status(401).send()
  }
  var currentStage = null
  console.log('\n.\n')
  System
    .findOne({})
    .populate('currentStage')
    .then((system) => {
      currentStage = system.currentStage
      return currentProjects()
    })
    .then((projects) => {
      console.log('T ', projects[0])
      var allHere = _.each(req.body.winners, (winner) => {
        return !!_.find(projects, { _id: winner })
      })
      if (!allHere) {
        console.error('no allHere')
        return res.status(401).send()
      }

      var passedWorks = _.map(req.body.winners, (winner) => {
        var t = _.find(projects, (project) => {
          return String(project.project._id) === winner
        })
        return { project: String(t.project._id), score: t.score }
      })

      return Stage.findByIdAndUpdate(currentStage._id, { passedWorks: passedWorks })
        .then(() => {
          return Stage.findById(currentStage._id)
            .then(() => {
              return Stage
                .findOne({ order: { $gt: currentStage.order }})
                .sort('order')
            })
            .then((nextStage) => {
              console.log('nextStage', nextStage)
              if (!nextStage) {
                return System
                  .findOneAndUpdate({}, { currentStage: null }, { new: true })
              }
              return System
                .findOneAndUpdate({}, { currentStage: nextStage }, { new: true })
            })
            .then((sys) => {
              console.log('sys', sys)
              res.status(200).json({ results: [] })
            })
        })
      // send event
    })
}
exports.getCurrentLikes = function (req, res) {
  Like.aggregate([
    { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'login' }},
    { $lookup: { from: 'projects', localField: 'project', foreignField: '_id', as: 'work' }},
    // {
    //   $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ['$login', 0] }, '$$ROOT"'] }}
    // },
    { $project: {
      _id: 0,
      username: {
        $arrayElemAt: ['$login.username', 0]
      },
      workid4: {
        $arrayElemAt: ['$work.id4', 0]
      },
      workid6: {
        $arrayElemAt: ['$work.id6', 0]
      },
      workid: {
        $arrayElemAt: ['$work._id', 0]
      },
      value: 1,
      like: { $cond: [{ $gt: ['$value', 0] }, 1, 0] },
      dislike: { $cond: [{ $lte: ['$value', 0] }, 1, 0] }
    }},
    {
      $group: {
        _id: '$username',
        likes: { $sum: '$like' },
        dislikes: { $sum: '$dislike' },
        works: { $push: { _id: '$workid', id4: '$workid4', id6: '$workid6', isLike: '$value' }},
        total: { $sum: 1 }
      }
    }
  ])
    .then((data) => {
      res.status(200).json({ results: data })
    })
}

function currentProjects () {
  var currentStage = null
  console.log('currentProjects')
  return System
    .findOne({})
    .populate('currentStage')
    .then((system) => {
      currentStage = system.currentStage
      return Stage
        .findOne({ order: { $lt: system.currentStage.order }})
        .sort('-order')
        .populate({
          path: 'passedWorks.project',
          select: '_id id4 id6 customLikes isFinalist',
          options: { lean: true }
        })
        .lean()
    })
    .then((prevStage) => {
      console.log('currentThings 2', prevStage)
      if (!prevStage) {
        return Project
          .find({ isSubmitted: true })
          .select('_id id4 id6 user customLikes isFinalist')
          .populate({
            path: 'user',
            select: 'id4 id6 email',
            options: { lean: true }
          })
          .lean()
          .then((projects) => {
            return _.map(projects, (project) => {
              return { project: project, score: 0 }
            })
          })
      }
      prevStage.passedWorks = _.reject(prevStage.passedWorks, (t) => {
        return !t.project
      })
      prevStage.passedWorks = _.uniqBy(prevStage.passedWorks, (t) => {
        return t.project._id
      })

      return _.map(prevStage.passedWorks, (project) => {
        return { project: project.project, score: 0 }
      })
      // return _.map(prevStage.passedWorks, (project) => {
      //   return { project: _.pick(project.project.toObject(), ['_id', 'pid']), score: 0 }
      // })
    })
    .then((projects) => {
      console.log('currentProjects t', projects.length)
      var projectsIds = _.map(projects, function (project) {
        return project.project._id
      })
      return likesByProjects(projectsIds, currentStage._id)
        .then((likes) => {
          _.each(projects, (project) => {
            var like = _.find(likes, { _id: project.project._id })
            project = _.assign(project, _.omit(like, '_id'))
          })
          return projects
        })
    })
}
// return Stage
//   .findOne({ order: 0 })
//   .populate('passedWorks.project')
// currentStage = prevStage
// return Project
//   .find({ isSubmitted: true })
// .select('_id id4 id6 isSubmitted')
// .lean()
function top100 () {
  var currentStage = null
  console.log('t1')
  return Stage.findById('5b58afccd4fcce78b90c6012')
    .then((prevStage) => {
      console.log('t1112', prevStage)
      currentStage = prevStage
      return Project
        .find({ isSubmitted: true })
        .select('_id id4 id6 isSubmitted')
        .lean()
    })
    .then((projects) => {
      console.log('t2', projects.length)
      return _.map(projects, (project) => {
        return { project: project, score: 0 }
      })
    })
    .then((projects) => {
      console.log('z')
      var projectsIds = _.map(projects, function (project) {
        return project.project._id
      })
      console.log('z2', currentStage, projectsIds)
      return likesByProjects(projectsIds, currentStage._id)
        .then((likes) => {
          console.log('t3', likes.length)
          _.each(projects, (project) => {
            var like = _.find(likes, { _id: project.project._id })
            project = _.assign(project, _.omit(like, '_id'))
          })
          return projects
        })
    })
}

function likesByProjects (projects, stageId) {
  console.log('zz1')
  console.log('zzz', Like, projects, stageId)
  return Like.aggregate([
    {
      $match: {
        project: { $in: projects },
        stage: stageId
      }
    }, {
      $group: {
        _id: '$project',
        likes: { '$sum': 1 },
        score: { '$sum': '$value' }
      }
    }
  ])
}
