var _ = require('lodash')


function paths(obj, pathToTrack, _parentKey) {
  const fields = pathToTrack.split('.')
  const currentKey = fields.shift();
  let parentKey = null;
  if (_parentKey) {
    if (currentKey){
      parentKey = `${_parentKey}.${currentKey}`;
    } else {
      parentKey = `${_parentKey}`;
    }
  } else {
    parentKey = currentKey
  }
  const restPath = fields.join('.')
  let result;
  const currentValue = obj[currentKey];
  if (_.isArray(currentValue)) {
    result = _.flatMap(currentValue, function (obj, i) {
      return paths(currentValue[i], restPath, (parentKey || '') + '[' + i + ']');
    });
  } else if  (_.isPlainObject(currentValue)) {
    return paths(currentValue, restPath, parentKey);
  } else {
    result = [parentKey];
  }
  return result
}

function replacePathToObject(target, path, obj){
  const pathArray = paths(target, path);
  _.each(pathArray, function(p) {
    _.set(target, p, obj)
  })
  return null;
}

module.exports =  {
  replacePathToObject,
  getAllPaths(obj, path){
    return paths(obj, path)
  }
}