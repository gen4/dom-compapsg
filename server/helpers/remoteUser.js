var request = require('request-promise')
const HOST = process.env.REMOTE_USER_SERVICE

// 'https://competition-test.conformity.io'
// process.env.NODE_ENV === 'production' ? 'https://dom-competition.ru' :
  // 'https://dom-competition.ru'

module.exports = function (qs) {
  const obj =
    {
      method: 'GET',
      uri: `${HOST}/api/getApplication`,
      qs,
      json: true
    }
    // if(process.env.NODE_ENV !== 'production'){
  obj.auth = {
    'user': 'test',
    'pass': 'test',
    'sendImmediately': false
  }
    // }
  return request(obj)
    .then((response) => {
      // console.log('user ', response)
      const newUser = {
        id6: response.data.id.value,
        lang: response.data.language.value,
        email: response.data.email.value,
        name: response.data['stageOne.LG8ArhD3Y2Fxv76N7.0.01-fullname'].value,
        submitted: response.data.submitted.value === 'submitted'
      }
      return newUser
    })
}
