var fs = require('fs')
var path = require('path')
var _ = require('lodash')
var pdf2img = require('pdf2img')
var createDirSync = require('./filesystem')
var config = require('../../config').backend

module.exports = function (input, id6) {
  const pathToSave = createDirSync(path.resolve(config.uploadDir, `${id6}`, `${id6}_albumConverted`))
  console.log('pathToSave', pathToSave)
  pdf2img.setOptions({
    type: 'jpg',                                // png or jpg, default jpg
    size: 2048,                                 // default 1024
    density: 600,                                // default 600
    outputdir: pathToSave,                      // output folder, default null (if null given, then it will create folder name same as file name)
    outputname: `${id6}_albumConverted`,        // output file name, dafault null (if null given, then it will create image name same as input name)
    page: null                                  // convert selected page, default null (if null given, then it will convert all pages)
  })

  const filepath = path.resolve(config.uploadDir, _.trimStart(input, '/'))
  console.log('>input', filepath, __dirname)
  return new Promise((resolve, reject) => {
    pdf2img.convert(filepath, function (err, info) {
      if (err) {
        console.log(err)
        return reject(err)
      }
      return resolve(info)
    })
  })
}
