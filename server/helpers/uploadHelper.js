'use strict'

var config = require('../../config').backend
var createDirSync = require('./filesystem')
var fs = require('fs')
var _ = require('lodash')
var path = require('path')
var multer = require('multer')
var multerS3 = require('multer-s3')
var AWS = require('aws-sdk')
var Upload = require('../../server/api/uploads/upload.model')
var Project = require('../../server/api/project/project.model')

AWS.config.update({
  region: 'ru-msk',
  endpoint: process.env.S3_HOST || 'http://ib.bizmrg.com'
})

var s3 = new AWS.S3()

var s3storage = multerS3({
  s3: s3,
  bucket: process.env.S3_BUCKET,
  acl: 'public-read',
  metadata: function (req, file, done) {
    console.log('meta', req.body.type, req.body.pid)
    done(null, { type: req.body.type, projectId: req.body.pid });
  },
  key: function (req, file, done) {
    if (!req.body.type) {
      return done('no file type provided')
    }
    const rnd = Math.random().toString(36).substring(7)
    const ext = _.takeRight(file.originalname.split('.'))
    const newFileName = `public/${req.body.pid}_${req.body.type}_${rnd}${Date.now()}.${ext}`
    console.log('> ', newFileName)
    done(null, newFileName)
  }
})

var storage = multer.diskStorage({
  destination: function (req, file, done) {
    const pid = req.body.pid
    if (!pid) {
      return done('no project id provided')
    }
    Project.findById(pid)
      .select('id6')
      .lean()
      .then((p) => {
        req.id6 = p.id6
        const pathToSave = createDirSync(path.resolve(config.uploadDir, p.id6 || pid))
        return done(null, pathToSave)
      })
  },
  filename: function (req, file, done) {
    if (!req.body.type) {
      return done('no file type provided')
    }
    const rnd = Math.random().toString(36).substring(7)
    const ext = _.takeRight(file.originalname.split('.'))
    const newFileName = `${req.id6 || req.body.pid}_${req.body.type}_${rnd}${Date.now()}.${ext}`
    console.log('> ', newFileName)
    done(null, newFileName)
  }
})

var upload = function (req, res, next) {
  // if (req.user && req.user.role === 'admin') {
  //   return multer({ storage: storage }).any()(req, res, (err) => { console.log('M - local'); req.ms3 = false; next(err) })
  // } else {
  return multer({ storage: s3storage }).any()(req, res, (err) => { console.log('M - s3 '); req.ms3 = true; next(err) })
 // }
}
// multer({ storage: s3storage })
exports.upload = upload // .any()
exports.s3 = s3
// exports.upload = function (req, res, next) {
//   uploadAnyMiddleware(req, res, (req, res) => catchUpload(req, res, next))
// }

