var handleErrorAsync = require('../api/errorhandler')
var _ = require('lodash')
var Promise = require('bluebird')

function Rest (Model) {
  this.Model = Model
  this.get = this.get.bind(this)
  this.list = this.list.bind(this)
  this.create = this.create.bind(this)
  this.update = this.update.bind(this)
  this.remove = this.remove.bind(this)
  // this.createWithFile = this.createWithFile.bind(this)
  // this.updateWithFile = this.updateWithFile.bind(this)
  this.applyRoutes = this.applyRoutes.bind(this)
}

Rest.prototype.get = function (req, res) {
  const id = req.params.id
  this.Model.findById(id)
    .lean()
    .then(function (results) {
      res.json(results)
    })
    .catch(handleErrorAsync(res))
}

Rest.prototype.list = function (req, res) {
  const count = +req.query.count || 20
  const page = +req.query.page || 0
  const search = req.query.search || {}
  const skip = +count * +page
  const limit = +count
  Promise.all([
    this.Model.find(search)
      .lean()
      .skip(skip)
      .limit(limit),
    this.Model.count(search)
  ])
    .spread(function (results, total) {
      res.json({ results: results, total: total })
    })
    .catch(handleErrorAsync(res))
}

Rest.prototype.create = function (req, res) {
  this.Model.create(req.body)
    .then(function (savedObject) {
      res.json(savedObject)
    })
    .catch(handleErrorAsync)
}

Rest.prototype.update = function (req, res) {
  const { id } = req.params
  const newObject = _.omit(req.body, ['_id', 'owner'])
  this.Model.findByIdAndUpdate(id, newObject)
    .then(function (obj) {
      res.json(obj)
    })
    .catch(handleErrorAsync(res))
}

Rest.prototype.remove = function (req, res) {
  const { id } = req.params;
  this.Model.findByIdAndRemove(id)
    .then(function (obj) {
      res.json(obj)
    })
    .catch(handleErrorAsync(res))
}

Rest.prototype.applyRoutes = function (url, router, mid){
  if (!mid) {
    mid = function (req, res, next) {
      next()
    }
  }
  router.get(`${url}/:id`, mid, this.get)
  router.get(`${url}`, mid, this.list)
  router.post(`${url}`, mid, this.create)
  router.put(`${url}/:id`, mid, this.update)
  router.delete(`${url}/:id`, mid, this.remove)
}

Rest.prototype.onlyForProjectOwnersOrAdmins = function (req, res, next) {
  next()
}
// Rest.prototype.createWithFile = function(aliases){ // fileField: fieldNameTo
//   return function (req, res, next) {
//     // if (req.files && req.files[0] && req.files[0].filename) {
//     //   req.body.url = `${req.files[0].filename}`
//     //   return this.create(req, res, next)
//     // }
//     // return res.status(400).json({ error: 'no file received' })
//   }
// }
//
// Rest.prototype.updateWithFile = function (req, res, next) {
//   if (req.files && req.files[0] && req.files[0].filename) {
//     const url = `${req.files[0].filename}`
//     this.Model.findByIdAndUpdate(req.params.id, { url: url })
//       .then(function (saved) {
//         return res.json(saved)
//       })
//       .catch(handleErrorAsync(res))
//   }
// }

module.exports = Rest
