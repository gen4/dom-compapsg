var fs = require('fs')
var path = require('path')

function createDirSync (pathToCreate) {
  return pathToCreate
    .split(path.sep)
    .reduce((currentPath, folder) => {
      currentPath += folder + path.sep
      if (!fs.existsSync(currentPath)) {
        fs.mkdirSync(currentPath)
      }
      return currentPath
    }, '')
}
module.exports = createDirSync
