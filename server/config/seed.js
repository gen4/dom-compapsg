/**
 * Populate DB with admin user data on server start
 */

'use strict'
var _ = require('lodash')
var Promise = require('bluebird');
var objectTool = require('../helpers/object-tool')
// var Sid = require('../api/sid/sid.model')
// var Checker = require('../api/checker/checker.model')
// var Project = require('../api/project/project.model')
var System = require('../api/system/system.model')
var Stage = require('../api/stage/stage.model')
// var Like = require('../api/like/like.model')
var ObjectId = require('mongoose').Types.ObjectId;

var Project = require('../api/project/project.model')
var projectInits = require('./initials/projects')

var User = require('../api/user/user.model')
var userInits = require('./initials/users')


var toSeed = {
  projects: projectInits.projects,
  users: userInits,
}
var seeded = {
  projects: [],
  users: []
}

function doSystem () {
  return System.findOne({})
    .lean()
    .then(function (sys) {
      if(!sys){
        Stage.findOne({})
          .sort('order')
          .lean()
          .then((stage)=>{
            return System.create({currentStage:stage})
          })
      }
      return null
    })
    .then(function() {
      console.log('finished populating system')
      return null;
    })
    .catch(errHandler)
}


function doCheckers(){
  return new Promise(function(resolve, reject){
    Checker.findOne({})
      .lean()
      .then(function(checker){
        // if(checker){
        //   return Promise.reject()
        // }
        return null
      })
      .then(function(){
        return Checker.remove({}).exec()
      })
      .then(function(){
        return Checker.collection.insert(checkers)
      })
      .then(function(){
        console.info('%d checkers were successfully stored.', checkers.length);
        return null;
      })
      .catch(errHandler)
      .finally(resolve)
  })
}



function errHandler(err){
  if(err){
    console.log(err)
  }
}


function doStages() {
  return new Promise(function (resolve, reject) {
    Stage.findOne({})
      .lean()
      .then(function (stage) {
        if (stage) {
          return Promise.reject()
        }
        return null
      })
      .then(function () {
       return Promise.all([
          Stage.create({
            dueDate: new Date().getTime() + 1000 * 60 * 5,
            order:0
          }),
          Stage.create({ order:1 }),
          Stage.create({ order:2 })
        ])
      })
      .then(function(){
        console.info('%d Stages were successfully stored.', 3);
        return null
      })
      .catch(errHandler)
      .finally(resolve)
  })
}

function setRefFields(obj, refClass, refField){
  const paths = objectTool.getAllPaths(obj, refField);
  _.each(paths, function (p) {
    const objToFind = _.get(obj, p);
    const objToRef = _.find(seeded[refClass], objToFind)
    _.set(obj, p, objToRef);
  })
}

function standardSeed (Model, name, refs) {
  console.log(' [s] ', name, toSeed[name])
  return Model.remove({})
    .then(function () {
      return Promise.each(toSeed[name], function (obj) {
        if (refs) {
          _.each(refs, function (refSeededClass, refField) {
            setRefFields(obj, refSeededClass, refField)
          })
        }
        const toSave = new Model(obj)
        return toSave.save()
          .then(function (saved) {
            seeded[name].push(saved)
            return null
          })
      })
    })
    .then(() => {
      console.log(`Seed: ${toSeed[name].length} ${name} seeded.`)
    })
    .catch((err) => {
      console.error(err)
    })
}

function doProjects () {
  return standardSeed(Project, 'projects')
}

function doUsers () {
  return standardSeed(User, 'users')
}

if (process.env.SHOULD_SEED && process.env.SHOULD_SEED === 'true') {
  doStages()
    .then(function () {
      return doSystem()
    })
    .then(function () {
      return doUsers()
    })
    // .then(function(){
    //   return doSids()
    // })
    // .then(function (){
    //   return doCheckers()
    // })
    // .then(doUploads)
    // .then(doProjects)
    .catch(errHandler)
// removeAll3Likes()
}

