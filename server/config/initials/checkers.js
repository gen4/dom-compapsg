module.exports = [
  {
    _id: 'avgCost',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'commonEvaluationFlat',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'commonEvaluationBuilding',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'commonEvaluationBlock',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'envType',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  //  --- --- SUB 1
  {
    _id: 'commonEvaluation',
    type: 'tri',
    grades: {
      l: [1, 2, 2, 2],
      m: [1, 2, 2, 2],
      h: [1, 2, 2, 2]
    }
  },
  {
    _id: 'area',
    type: 'num',
    grades: {
      l: [1.9, 2, 4, 4.2], //4.2
      m: [1.9, 2, 3, 3.2],
      h: [0.6, 0.6, 1.5, 1.65]
    }
  },
  {
    _id: 'length',
    type: 'num',
    grades: {
      l: [190, 200, 300, 315],
      m: [126, 140, 200, 210],
      h: [75, 75, 150, 165]
    }
  },
  {
    _id: 'height',
    type: 'range',
    grades: {
      l: [1, 1, 4, 4],
      m: [4, 5, 7, 8],
      h: [5, 5, 9, 10]
    }
  },
  {
    _id: 'developedPercent',
    type: 'num',
    grades: {
      l: [19, 20, 25, 27],
      m: [22, 25, 30, 35],
      h: [30, 30, 40, 50]
    }
  },
  {
    _id: 'dens',
    type: 'num',
    grades: {
      l: [4000, 4000, 6000, 6300],
      m: [13500, 15000, 20000, 22000],
      h: [17100, 18000, 25000, 26250]
    }
  },
  {
    _id: 'commers',
    type: 'num',
    grades: {
      l: [5, 5, 10, 12],
      m: [5, 5, 20, 22],
      h: [20, 20, 40, 45]
    }
  },
  {
    _id: 'parking',
    type: 'num',
    grades: {
      l: [335, 355, 395, 415],
      m: [212, 223, 247, 259],
      h: [135, 143, 158, 165]
    }
  },
  {
    _id: 'population',
    type: 'num',
    grades: {
      l: [45, 50, 80, 90],
      m: [270, 300, 450, 495],
      h: [360, 400, 450, 500]
    }
  },
  {
    _id: 'edu',
    type: 'bol',
    grades: {
      l: [0.5, 0.5, 1.5, 2],
      m: [0.5, 0.5, 1.5, 2],
      h: [0.5, 0.5, 1.5, 2],
    }
  },


//  --- --- SUB 2


  {
    _id: 'buildingHeight.0',
    type: 'num',
    grades: {
      l: [1, 1, 2, 2],
      m: [4, 5, 7, 8],
      h: [5, 5, 9, 10]
    }
  },
  {
    _id: 'buildingHeight.1',
    type: 'num',
    grades: {
      l: [1, 1, 2, 2],
      m: [4, 5, 7, 8],
      h: [5, 5, 9, 10]
    }
  },
  {
    _id: 'buildingHeight.2',
    type: 'num',
    grades: {
      l: [1, 1, 2, 3],
      m: [4, 5, 7, 8],
      h: [8, 8, 18, 18]
    }
  },
  {
    _id: 'buildingHeight.3',
    type: 'num',
    grades: {
      l: [2, 2, 4, 4]
    }
  },
  {
    _id: 'm2price.0',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300],
      m: [1, 1, 30400, 39000],
      h: [1, 1, 33500, 43400]
    }
  },
  {
    _id: 'm2price.1',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300],
      m: [1, 1, 30400, 39000],
      h: [1, 1, 33500, 43400]
    }
  },
  {
    _id: 'm2price.2',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300],
      m: [1, 1, 30400, 39000],
      h: [1, 1, 33500, 43400]
    }
  },
  {
    _id: 'm2price.3',
    type: 'num',
    grades: {
      l: [1, 1, 30500, 34300]
    }
  },
//
  {
    _id: 'floorArea.0',
    type: 'num',
    grades: {
      l: [75, 80, 120, 130],
      m: [300, 350, 450, 475],
      h: [375, 450, 550, 600]
    }
  },
  {
    _id: 'floorArea.1',
    type: 'num',
    grades: {
      l: [75, 80, 120, 130],
      m: [375, 450, 550, 625],
      h: [375, 450, 600, 625]
    }
  },
  {
    _id: 'floorArea.2',
    type: 'num',
    grades: {
      l: [55, 60, 100, 110],
      m: [375, 450, 600, 625],
      h: [450, 500, 550, 600]
    }
  },
  {
    _id: 'floorArea.3',
    type: 'num',
    grades: {
      l: [300, 350, 450, 475]
    }
  },
  //

  {
    _id: 'flatsQty.0',
    type: 'range',
    grades: {
      m: [2, 4, 6.5, 8.5],
      h: [2, 3, 8.5, 8.5]
    }
  },
  {
    _id: 'flatsQty.1',
    type: 'range',
    grades: {
      m: [3, 4, 8.5, 8.5],
      h: [4, 6, 8.5, 8.5]
    }
  },
  {
    _id: 'flatsQty.2',
    type: 'range',
    grades: {
      m: [4, 6, 8.5, 8.5],
      h: [3, 4, 8.5, 8.5]
    }
  },
  {
    _id: 'flatsQty.3',
    type: 'range',
    grades: {
      l: [2, 2, 4, 8]
    }
  },

  //

  {
    _id: 'elevatorsQty.0',
    type: 'num',
    grades: {
      m: [1, 1, 2, 4],
      h: [2, 2, 4, 4]
    }
  },
  {
    _id: 'elevatorsQty.1',
    type: 'num',
    grades: {
      m: [1, 2, 3, 4],
      h: [2, 2, 4, 4]
    }
  },
  {
    _id: 'elevatorsQty.2',
    type: 'num',
    grades: {
      m: [1, 2, 3, 4],
      h: [2, 2 ,4, 4]
    }
  },
  {
    _id: 'elevatorsQty.3',
    type: 'num',
    grades: {
      l: [1, 1, 2, 3]
    }
  },

  //  --- --- SUB 3
  {
    _id: 'area.0',
    type: 'num',
    grades: {
      l: [19, 19, 21, 25],
      m: [19, 19, 21, 25],
      h: [19, 19, 21, 25]
    }
  },
  {
    _id: 'area.1',
    type: 'num',
    grades: {
      l: [35, 39, 41, 45],
      m: [35, 39, 41, 45],
      h: [35, 39, 41, 45]
    }
  },
  {
    _id: 'area.2',
    type: 'num',
    grades: {
      l: [55, 59, 61, 65],
      m: [55, 59, 61, 65],
      h: [55, 59, 61, 65]
    }
  },
  {
    _id: 'area.3',
    type: 'num',
    grades: {
      l: [75, 79, 81, 85],
      m: [75, 79, 81, 85],
      h: [75, 79, 81, 85]
    }
  },
  {
    _id: 'area.4',
    type: 'num',
    grades: {
      l: [95, 99, 101, 105],
      m: [95, 99, 101, 105],
      h: [95, 99, 101, 105]
    }
  }
  ]