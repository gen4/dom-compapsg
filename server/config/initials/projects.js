const _ = require('lodash')
const extensions = {
  image: 'png,jpg,jpeg',
  pdf: 'pdf',
  doc: 'doc,docx',
  pdfDoc: 'pdf,doc,docx',
  imagePdfDoc: 'png,jpg,jpeg,pdf,doc,docx',
  dwg: ''
}
const accepts = {
  image: 'image/png,image/gif,image/jpeg,image/webp',
  pdf: 'application/pdf',
  doc: '.doc,.docx',
  pdfDoc: 'application/pdf,.doc,.docx',
  imagePdfDoc: 'image/png,image/gif,image/jpeg,image/webp,application/pdf,.doc,.docx',
  dwg: '.dwg'// drawing/x-dwf,model/vnd.dwf
}
const vis = {
  album: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    sizeLimit: 25,
    extensions: extensions.pdf,
    accept: accepts.pdf,
    filesLimit: 1
  },
  panel1: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 10,
    filesLimit: 1
  },
  panel2: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 10,
    filesLimit: 1
  },
  albumPrint: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    sizeLimit: 25,
    extensions: extensions.pdf,
    accept: accepts.pdf,
    filesLimit: 1
  },
  panel1Print: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.pdf,
    accept: accepts.pdf,
    sizeLimit: 20,
    filesLimit: 1
  },
  panel2Print: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.pdf,
    accept: accepts.pdf,
    sizeLimit: 20,
    filesLimit: 1
  },
  cover: {
    isHidden: true,
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  schemas: {
    isRequired: false,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 5
  },
  decl: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.imagePdfDoc,
    accept: accepts.imagePdfDoc,
    sizeLimit: 5,
    filesLimit: 1
  },
  consent: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.imagePdfDoc,
    accept: accepts.imagePdfDoc,
    sizeLimit: 5,
    filesLimit: 1
  },
  note: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.doc,
    accept: accepts.doc,
    sizeLimit: 5,
    filesLimit: 1
  },
  blueprint: {
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.dwg,
    accept: accepts.dwg,
    sizeLimit: 10,
    filesLimit: 10
  },

  // VIS - - -
  baseVis: {
    // name: {
    //   ru: 'Визуализация 1',
    //   en: 'Visualisation 1'
    // },
    // isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 10,
    filesLimit: 1
  },
  // vis2: {
  //   name: {
  //     ru: 'Визуализация 2',
  //     en: 'Visualisation 2'
  //   },
  //   isRequired: true,
  //   help: {
  //     ru: '',
  //     en: ''
  //   },
  //   extensions: extensions.image,
  //   sizeLimit: 10,
  //   filesLimit: 1
  // },
  // vis3: {
  //   name: {
  //     ru: 'Визуализация 3',
  //     en: 'Visualisation 3'
  //   },
  //   isRequired: true,
  //   help: {
  //     ru: '',
  //     en: ''
  //   },
  //   extensions: extensions.image,
  //   sizeLimit: 10,
  //   filesLimit: 1
  // },
  // vis4: {
  //   name: {
  //     ru: 'Визуализация 4',
  //     en: 'Visualisation 4'
  //   },
  //   isRequired: true,
  //   help: {
  //     ru: '',
  //     en: ''
  //   },
  //   extensions: extensions.image,
  //   sizeLimit: 10,
  //   filesLimit: 1
  // },
  // vis5: {
  //   name: {
  //     ru: 'Визуализация 5',
  //     en: 'Visualisation 5'
  //   },
  //   isRequired: true,
  //   help: {
  //     ru: '',
  //     en: ''
  //   },
  //   extensions: extensions.image,
  //   sizeLimit: 10,
  //   filesLimit: 1
  // },
  groundFloor: {
    name: {
      ru: 'План 1-го этажа',
      en: 'Ground floor plan'
    },
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  standardFloor: {
    name: {
      ru: 'План типового этажа',
      en: 'Standard floor plan'
    },
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  section1: {
    name: {
      ru: 'Разрез 1-1',
      en: 'Section 1-1'
    },
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  section2: {
    name: {
      ru: 'Разрез 2-2',
      en: 'Section 2-2'
    },
    isRequired: false,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  facade1: {
    name: {
      ru: 'Фасад 1',
      en: 'Facade 1'
    },
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  facade2: {
    name: {
      ru: 'Фасад 2',
      en: 'Facade 2'
    },
    isRequired: false,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  scheme1: {
    name: {
      ru: 'Схема 1, иллюстрирующая принципиальные решения',
      en: 'Scheme 1 illustrating principal solutions'
    },
    isRequired: false,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  catalogue: {
    name: {
      ru: 'Квартирография',
      en: 'Catalogue of apartments'
    },
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  // flats --- --- --- --- ---- ----
  apartmentPlan: {
    name: {
      ru: 'План',
      en: 'Apartment plan'
    },
    isRequired: true,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  },
  interior: {
    name: {
      ru: 'Визуализация',
      en: 'Interior vizualization'
    },
    isRequired: false,
    help: {
      ru: '',
      en: ''
    },
    extensions: extensions.image,
    accept: accepts.image,
    sizeLimit: 5,
    filesLimit: 1
  }
}

const notes = {
  floorArea: {
    name: {
      ru: '* Площадь этажа здания',
      en: '* Floor area'
    },
    help: {
      ru: 'Измеряется по внутренними поверхностям ограждающих конструкций наружных стен (осей крайних колонн, при отсутствии наружных стен) на уровне пола без учета плинтусов\n' +
      '\n' +
      'включает:\n' +
      '- сумму площадей помещений (комнат) всех функциональных типов, а также внутридомовых строительных конструкций со встроенными каналами и шахтами инженерного оборудования\n' +
      '- площадь помещений лестничных клеток внутриквартирных лестниц (пандусов) во внутреннем контуре помещения лестничной клетки (в размерах площадей лестничных площадок и горизонтальных проекций наклонных маршей лестниц, пандусов)',
      en: 'Area of the floor within the inside perimeter of the exterior walls of the building (axis of columns if there are no external walls)\n' +
      '\n' +
      'includes: \n' +
      '- areas of all rooms of all functional types on the floor as well as interior walls, shafts and other construction elements inside building volume\n' +
      '- areas of apartment stairwells (ramps) within the inside perimeter of the stairwell (in size of staircase area and horizontal projections of flight of stairs, ramps)'
    },
    type: 'note',
    isVisible: true
  },
  loweringFactors: {
    name: {
      ru: '** Понижающие коэффиценты',
      en: '** Lowering factors'
    },
    help: {
      ru: 'Площадь летних помещений подсчитывается путем умножения фактической площади на следующие понижающие коэффициенты:\n' +
      '\n' +
      '- балкон или терраса 0.3\n' +
      '- лоджия 0.5\n' +
      '',
      en: 'The area of sunrooms should be decreased by following coefficients:\n' +
      '\n' +
      '- balcony or terrace 0.3\n' +
      '- loggia 0.5\n' +
      ''
    },
    type: 'note',
    isVisible: true
  },
  measureARoom: {
    name: {
      ru: '* Правила подсчета площади помещения',
      en: '* How to measure a room area'
    },
    help: {
      ru: 'Определяется по размерам, измеряемым между поверхностями ограждающих конструкций на уровне пола\n' +
      '\n' +
      'включает:\n' +
      '- ниши высотой 2м и более, арочные проемы шириной 2м и более;\n' +
      '- помещения с наклонными стенами и потолками. При высоте помещения менее 2,3 м (но более 1,1 м) применяется понижающий коэффициент 0,7.\n' +
      '\n' +
      'не включает:\n' +
      '- несущие и ограждающие конструктивные элементы и шахты;\n' +
      '- нишы высотой менее 2м, арочные проёмы шириной менее 2м;\n' +
      '- дверные проемы в контурах стен;\n' +
      '- камины;\n' +
      '- помещения с наклонными и разновысотными ограждающими конструкциями стен и потолков при высоте до 1,1 м\n' +
      '',
      en: 'Area of the room should be measured flush with floor level between wall surfaces and listed on plan drawings\n' +
      '\n' +
      'includes:\n' +
      '- niches 2m high and more, archways 2m wide and more;\n' +
      '- area with sloped ceiling or inclined walls and area with varied ceiling height should be decreased by coefficient 0.7 if height of this spaces is less than 2.3m and more than 1.1m.\n' +
      ' \n' +
      'excludes:\n' +
      '- construction elements and shafts;\n' +
      '- niches less than 2m high archways less than 2m wide;\n' +
      '- door openings area;\n' +
      '- fireplaces\n' +
      '- area with sloped ceiling or inclined walls and area with varied ceiling height if height of this spaces is less than 1.1m\n' +
      ''
    },
    type: 'note',
    isVisible: true
  },
  measureAnAp: {
    name: {
      ru: '* Правила подсчета площади в помещениях с внутриквартирной лестницей',
      en: '* How to measure an area in the apartment with staircase'
    },
    help: {
      ru: 'Площадь пола под маршем внутриквартирной лестницы, при высоте пространства 1,6 м и более, а также площадь лестничных площадок в уровне этажа включаются в площадь помещения, в котором расположена лестница. Остальная площадь под маршем внутриквартирной лестницы включается в общую площадь квартиры как нежилая\n' +
      '\n' +
      'Площадь проема в перекрытии в площади квартиры не учитывается',
      en: 'Area under the stairs with height more than 1,6 m and area of landings which have same elevation as floors are included in the surrounding room area. The rest of the area includes in total as non-living area\n' +
      '\n' +
      'The area of ​​the opening in the floor is not taken included'
    },
    type: 'note',
    isVisible: true
  },
  typeOfSunroom: {
    name: {
      ru: '** Правила определения типа летнего помещения (балкона/террасы/лоджии)',
      en: '** How to identify the type of sunroom (balcony/loggia/terrace)'
    },
    help: {
      ru: 'Балкон – это помещение открытое внешнему пространству, полностью или более чем на половину площади выступающей из контура поверхностей граничащих с ней наружных стен\n' +
      '\n' +
      'Терраса – это  помещение в виде площадки, расположенной на земле или на эксплуатируемой кровле, примыкающее к наружным стенам \n' +
      '\n' +
      'Лоджия – это помещение открытое внешнему пространству, полностью или более чем на половину площади в контуре поверхностей граничащих с ней наружных стен',
      en: 'Balcony is a sunroom ledged to the outside (beyond contours of surfaces of external walls) more than a half of its area\n' +
      '\n' +
      'Terrace is a sunroom placed on the ground or on the roof, borders on external walls or roofs \n' +
      '\n' +
      'Loggia is a sunroom ledged to the outside (beyond contours of surfaces of external walls) less than a half of its area'
    },
    type: 'note',
    isVisible: true
  }
}
// urbanVilla
// sectionBuilding
// galleryBuilding
// tower
// studio25
// oneBedAp45
// oneBedAp65
// twoBedAp85
// threeBedAp105
// studio30
// oneBedAp50
// oneBedAp70
// twoBedAp90
// threeBedAp110
const teps = {

  numberOfStoreys: {
    name: {
      ru: 'Общее количество этажей (шт.)',
      en: 'Number of storeys (storey)'
    },
    help: {
      ru: 'Включает количество надземных этажей, в том числе этажи цокольные при расположении поверхности их перекрытий выше средней планировочной отметки уровня земли не менее чем на 2 метра\n' +
      '\n'+
      'Если отдельные части здания имеют разное количество надземных этажей, то необходимо указать минимальное и максимальное значение',
      en: 'Number of storeys above ground which includes semi-basement storey with ceiling slab no less than 2 meters above ground\n' +
      '\n'+
      'If parts of the building have different numbers of storeys, it is necessary to input min and max number'
    },
    type: 'range',
    precision: 0,
    limits: {
      urbanVilla: {
        colors: [3, 4, 7, 8],
        target: [4, 7]
      },
      sectionBuilding: {
        colors: [4, 5, 9, 9],
        target: [5, 9]
      },
      galleryBuilding: {
        colors: [4, 5, 9, 9],
        target: [5, 9]
      },
      tower: {
        colors: [9, 9, 18, 18],
        target: [9, 18]
      }
    },
    isRequired: true,
    isVisible: true
  },
  numberOfNonResidentialStoreys: {
    name: {
      ru: 'Количество нежилых этажей (шт.)',
      en: 'Number of non-residential storeys (storey)'
    },
    help: {
      ru: 'Количество этажей полностью или частично занятых помещениями общественного назначения',
      en: 'Number of storeys fully or partly composed of non-residental facilities'
    },
    precision: 0,
    type: 'num',
    couldByZero: true,
    isRequired: true,
    isVisible: true
  },
  buildingFootprint: {
    name: {
      ru: 'Площадь застройки здания (0.0 м²)',
      en: 'Building footprint (0.0 m²)'
    },
    help: {
      ru: 'Определяется как площадь горизонтального сечения по внешнему контуру наружных стен здания на уровне цоколя',
      en: 'Is calculated as overground building footprint area measured to the outer perimeter of external construction features'
    },
    type: 'num',
    precision: -1,
    limits: {
      urbanVilla: {
        colors: [350, 390, 510, 550],
        target: [400, 500]
      },
      sectionBuilding: {
        colors: [400, 440, 560, 600],
        target: [450, 550]
      },
      galleryBuilding: {
        colors: [450, 490, 660, 700],
        target: [500, 650]
      },
      tower: {
        colors: [450, 490, 660, 700],
        target: [500, 650]
      }
    },
    isRequired: true,
    isVisible: true
  },
  netArea: {
    name: {
      ru: 'Площадь жилой части здания (0.0 м²)',
      en: 'Net area (0.0 m²)'
    },
    help: {
      ru: 'Сумма общих площадей этажей* здания, измеренных в пределах внутренних поверхностей наружных стен\n' +
      '\n' +
      'Включает:\n' +
      '- площади балконов, лоджий, террас и эксплуатируемых кровель, веранд, лестничных площадок и ступеней в уровне каждого этажа (с учетом понижающих коэффициентов**)\n' +
      '- площадь лифтовых и других шахт и проёмов многосветных помещений в пределах только одного (нижнего) этажа\n' +
      '\n' +
      'Не включает:\n' +
      '- нежилые этажи, технические помещения, крыльца, наружные открытые лестницы, чердаки, подвалы и гаражи\n' +
      '- помещения с высотой менее 1,8 м\n' +
      '- пристройки к основному объему здания\n' +
      '- элементы ландшафта',
      en: 'The sum of the areas of each floor level* within the inside perimeter of the exterior walls of the building\n' +
      '\n' +
      'Includes:\n' +
      '- areas of balconies, loggias, terraces, verandas, trafficable roofs, staircases on each floor (with lowering factors**)\n' +
      '- shafts area only on the lower floor\n' +
      '\n' +
      'Excludes: \n' +
      '- areas with non-residental facilities, technical rooms, entrance porches, external staircases, attics, basements and parkings\n' +
      '- areas with ceiling height less than 1.8 meters\n' +
      '- annexes to the main structure\n' +
      '- landscape elements\n'
    },
    precision: -1,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  grossArea: {
    name: {
      ru: 'Общая площадь здания (0.0 м²)',
      en: 'Gross area (0.0 m²)'
    },
    help: {
      ru: 'Площадь жилой части здания плюс сумма площадей нежилых и технических этажей, подвалов (полуподвалов) и чердаков, измеренных в пределах внутренних поверхностей наружных стен\n' +
      '\n' +
      'Включает:\n' +
      '- площади балконов, лоджий, террас и эксплуатируемых кровель, веранд, лестничных площадок и ступеней в уровне каждого этажа (с учетом понижающих коэффициентов**)\n' +
      '- площадь лифтовых и других шахт и проёмов многосветных помещений в пределах только одного (нижнего) этажа\n' +
      '\n' +
      'Не включает:\n' +
      '- чердаки и технические помещения\n' +
      '- помещения с высотой менее 1,8 м\n' +
      '- пристройки к основному объему здания\n' +
      '- элементы ландшафта',
      en: 'Is calculated as the total area of residential part plus the sum of the areas of non-residential and technical floors, cellars and attics measured within the interior surfaces of the external walls\n' +
      '\n' +
      'Includes:\n' +
      '- areas of balconies, loggias, terraces, verandas, trafficable roofs, staircases on each floor (with lowering factors**)\n' +
      '- shafts area only on the lower floor\n' +
      '\n' +
      'Excludes: \n' +
      '- attics and technical rooms\n' +
      '- areas with ceiling height less than 1.8 meters\n' +
      '- annexes to the main structure\n' +
      '- landscape elements'
    },
    precision: -1,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  nonResidentialArea: {
    name: {
      ru: 'Площадь нежилых помещений (0.0 м²)',
      en: 'Non-residential area (0.0 m²)'
    },
    help: {
      ru: 'Сумма площадей, занятая помещениями общественного назначения, во всем здании',
      en: 'The sum of the areas with non-residential facilities'
    },
    precision: -1,
    type: 'num',
    couldByZero: true,
    isRequired: true,
    isVisible: true
  },
  collectiveSpaces: {
    name: {
      ru: 'Площадь коллективных пространств для жителей (0.0 м²)',
      en: 'Collective spaces for residents area (0.0 m²)'
    },
    help: {
      ru: 'Сумма площадей мест общего пользования, предназначенных для рекреации внутри здания',
      en: 'The sum of the areas of collective spaces inside of the building dedicated for recreational use'
    },
    precision: -1,
    type: 'num',
    couldByZero: true,
    isRequired: true,
    isVisible: true
  },
  apartmentsPerFloor: {
    name: {
      ru: 'Количество квартир на этаже (шт.)',
      en: 'Number of apartments per floor (units)'
    },
    help: {
      ru: 'Минимальное и максимальное количество квартир на этаже здания, при всех возможных комбинациях расположения квартир на этаже',
      en: 'The minimum and maximum number of apartments per floor in all possible combinations'
    },
    precision: 0,
    type: 'range',
    limits: {
      urbanVilla: {
        colors: [3, 3, 5, 8],
        target: [3, 5]
      },
      sectionBuilding: {
        colors: [4, 4, 8, 10],
        target: [4, 8]
      },
      galleryBuilding: {
        colors: [5, 6, 16, 16],
        target: [6, 16]
      },
      tower: {
        colors: [3, 4, 8, 10],
        target: [4, 8]
      }
    },
    isRequired: true,
    isVisible: true
  },
  apartmentsPerBuilding: {
    name: {
      ru: 'Количество квартир в доме (шт.)',
      en: 'Number of apartments in the building (units)'
    },
    help: {
      ru: 'Количество квартир во всем здании',
      en: 'Overall number of apartments in the building'
    },
    precision: 0,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  studioPercent: {
    name: {
      ru: '\t\tстудии, 25 м² (%)',
      en: '\t\tstudio 25 m², (%)'
    },
    help: {
      ru: 'Процент от общего количества квартир',
      en: 'As percentage of number of apartments in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  oneBedroomAps45: {
    name: {
      ru: '\t\tквартиры с 1-ой спальней, 45 м² (%)',
      en: '\t\t1-bedroom apartments, 45 m² (%)'
    },
    help: {
      ru: 'Процент от общего количества квартир',
      en: 'As percentage of number of apartments in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  oneBedroomAps65: {
    name: {
      ru: '\t\tквартиры с 1-ой спальней, 65 м² (%)',
      en: '\t\t1-bedroom apartments, 65 m² (%)'
    },
    help: {
      ru: 'Процент от общего количества квартир',
      en: 'As percentage of number of apartments in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  twoBedroomAps85: {
    name: {
      ru: '\t\tквартиры с 2-мя спальнями, 85 м² (%)',
      en: '\t\t2-bedroom apartments, 85 m² (%)'
    },
    help: {
      ru: 'Процент от общего количества квартир',
      en: 'As percentage of number of apartments in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  threeBedroomAps105: {
    name: {
      ru: '\t\tквартиры с 3-мя спальнями, 105 м² (%)',
      en: '\t\t3-bedroom apartments, 105 m² (%)'
    },
    help: {
      ru: 'Процент от общего количества квартир',
      en: 'As percentage of number of apartments in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  uniqueAps: {
    name: {
      ru: '\t\tуникальные квартиры (%)',
      en: '\t\tunique apartments (%)'
    },
    help: {
      ru: 'Процент от общего количества квартир',
      en: 'As percentage of number of apartments in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  totalApsArea: {
    name: {
      ru: 'Площадь квартир (0.0 м²)',
      en: 'Total area of apartments (0.0 m²)'
    },
    help: {
      ru: 'Сумма общих площадей квартир во всем здании',
      en: 'The sum of total areas of all apartments in the building'
    },
    precision: -1,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  floorHeight: {
    name: {
      ru: 'Высота этажа (0.00 м)',
      en: 'Floor height (0.00 m)'
    },
    help: {
      ru: 'Расстояние от верха нижерасположенного перекрытия до верха расположенного над ним перекрытия',
      en: 'The vertical distance between floor finish levels of two stories'
    },
    precision: 2,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  ceilingHeight: {
    name: {
      ru: 'Высота потолка (0.00 м)',
      en: 'Ceiling height (0.00 m)'
    },
    help: {
      ru: 'Расстояние от верха нижерасположенного перекрытия до низа расположенного над ним перекрытия',
      en: 'The vertical distance between floor finish level and ceiling finish level on the same storey'
    },
    precision: 2,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  sectionLength: {
    name: {
      ru: 'Длина секции (0.0 м)',
      en: 'Length of the section (0.0 m)'
    },
    help: {
      ru: 'Длина одной секции здания',
      en: 'Length of the longest facade of the building'
    },
    precision: 1,
    type: 'num',
    limits: {
      galleryBuilding: {
        colors: [35, 40, 50, 55],
        target: [40, 40]
      }
    },
    isRequired: true,
    isVisible: true
  },
  activeFacades: {
    name: {
      ru: 'Количество активных фасадов (шт.)',
      en: 'Number of active facades (units)'
    },
    help: {
      ru: 'Количество фасадов, имеющих оконные или дверные проемы',
      en: 'Number of active facades with windows or door openings'
    },
    precision: 0,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  elevators: {
    name: {
      ru: 'Количество лифтов (шт.)',
      en: 'Number of elevators (units)'
    },
    help: {
      ru: 'Количество лифтов во всем здании',
      en: 'Number of elevators in the building'
    },
    precision: 0,
    couldByZero: true,
    type: 'num',
    limits: {
      urbanVilla: {
        colors: [1, 1, 1, 2],
        target: [1, 1]
      },
      sectionBuilding: {
        colors: [1, 1, 2, 2],
        target: [1, 2]
      },
      galleryBuilding: {
        colors: [1, 1, 2, 2],
        target: [1, 2]
      },
      tower: {
        colors: [1, 2, 2, 3],
        target: [2, 2]
      }
    },
    isRequired: true,
    isVisible: true
  },
  serviceElevator: {
    name: {
      ru: 'Грузовой лифт (да/нет)',
      en: 'Service elevator (yes/no)'
    },
    help: {
      ru: 'Наличие лифта для транспортировки крупногабаритных предметов (оптимальные габариты кабины лифта - 1,8 х 2,7 м)',
      en: 'At least one elevator can lift large objects (optimal dimensions of elevator cabin - 1,8 m х 2,7 m)'
    },
    type: 'bol',
    isRequired: true,
    isVisible: true
  },
  twoSidedOrientation: {
    name: {
      ru: 'Наличие квартир с двухсторонней ориентацией (да/нет)',
      en: 'Apartments with two-sided orientation (yes/no)'
    },
    help: {
      ru: 'Наличие квартир с двухсторонней ориентации',
      en: 'Presence of apartments with two-sided orientation'
    },
    type: 'bol',
    isRequired: true,
    isVisible: true
  },
  conciergeRoom: {
    name: {
      ru: 'Помещения для консьержа (да/нет)',
      en: 'Concierge room (yes/no)'
    },
    help: {
      ru: 'Наличие помещений для консьержа',
      en: 'Special room with facilities for a concierge'
    },
    type: 'bol',
    isRequired: true,
    isVisible: true
  },
  transparentEntrance: {
    name: {
      ru: 'Сквозные подъезды (да/нет)',
      en: 'Transparent pass-through entrance (yes/no)'
    },
    help: {
      ru: 'Наличие сквозного подъезда ориентированного на два противоположных фасада здания',
      en: 'Transparent pass-through entrance with orientation to opposite facades'
    },
    type: 'bol',
    isRequired: true,
    isVisible: true
  },
  twoStoreyAps: {
    name: {
      ru: 'Двухэтажные квартиры (да/нет)',
      en: 'Two-storey apartments (yes/no)'
    },
    help: {
      ru: 'Наличие двухэтажных квартир в здании',
      en: 'Presence of two-storey apartments in the building'
    },
    type: 'bol',
    isRequired: true,
    isVisible: true
  },
  storageSpace: {
    name: {
      ru: 'Места хранения (да/нет)',
      en: 'Collective storage spaces (yes/no)'
    },
    help: {
      ru: 'Наличие организованных пространств в местах общего пользования внутри здания предназначенных для хранения личных вещей жильцов',
      en: 'Presence of collective storage spaces inside the building that are intended for storing personal belongings of the residents'
    },
    type: 'bol',
    isRequired: true,
    isVisible: true
  },
  k1: {
    name: {
      ru: 'К1',
      en: 'К1'
    },
    help: {
      ru: 'Коэффициент использования жилой части здания определяется отношением:\n' +
      '\n' +
      'Площадь жилой части здания (м²) / Площадь квартир (м²)',
      en: 'Residental use index is calculated as:\n' +
      '\n' +
      'Net area (m²) / Total area of apartments (m²)'
    },
    precision: 2,
    type: 'numCalculated',
    isRequired: false,
    isVisible: true
  },
  k2: {
    name: {
      ru: 'К2',
      en: 'К2'
    },
    help: {
      ru: 'Коэффициент использования здания определяется отношением: \n' +
      '\n' +
      'Общая площадь здания (м²) / Площадь квартир (м²)',
      en: 'Building use index is calculated as: \n' +
      '\n' +
      'Gross area (m²) / Total area of apartments (m²)'
    },
    precision: 2,
    type: 'numCalculated',
    isRequired: false,
    isVisible: true
  },
  // FLATS -- -- -- -- -- -- -- --- -- -- -- -- --
  livingArea: {
    name: {
      ru: 'Жилая площадь* (0.00 м²)',
      en: 'Living area* (0.00 m²)'
    },
    help: {
      ru: 'Определяется суммой площадей всех жилых помещений (гостиных и спален)\n' +
      '\n' +
      'При объединении гостиная-столовая-кухня следует выделить площадь кухни как нежилую, графически обозначив её границу на плане',
      en: 'The sum of all living rooms areas (living rooms, bedrooms)\n' +
      '\n' +
      'In case of merged living-dinning-kitchen rooms, kitchen should be marked as non-living area and bounded on the floor plan'
    },
    precision: 2,
    type: 'num',
    // studio25
// oneBedAp45
// oneBedAp65
// twoBedAp85
// threeBedAp105
// studio30
// oneBedAp50
// oneBedAp70
// twoBedAp90
// threeBedAp110

    isRequired: true,
    isVisible: true
  },
  nonLivingArea: {
    name: {
      ru: 'Нежилая площадь* (0.00 м²)',
      en: 'Non-living area* (0.00 m²)'
    },
    help: {
      ru: 'Cумма площадей всех нежилых помещений',
      en: 'The sum of all non-living rooms area'
    },
    precision: 2,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  sunroomsAreaBT: {
    name: {
      ru: 'Площадь летних помещений (балконы, террасы) (0.00 м²)',
      en: 'Sunrooms area (balconies, terraces) (0.00 m²)'
    },
    help: {
      ru: 'Сумма площадей балконов/террас/лоджий определяется по размерам, измеряемым по внутреннему контуру между фасадной поверхностью наружной стены и внутренней поверхностью ограждения открытого помещения без учета площади, занятой этим ограждением',
      en: 'The sum of sunroom area should be measured as a space bounded by facade surface of the exterior wall and inner side of sunroom fence without taking into account area that is covered by the fence'
    },
    precision: 2,
    hideIfNull: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  sunroomsAreaLoggies: {
    name: {
      ru: 'Площадь летних помещений (лоджии)** (0.00 м²)',
      en: 'Sunrooms area (loggias)** (0.00 m²)'
    },
    help: {
      ru: 'Сумма площадей балконов/террас/лоджий определяется по размерам, измеряемым по внутреннему контуру между фасадной поверхностью наружной стены и внутренней поверхностью ограждения открытого помещения без учета площади, занятой этим ограждением',
      en: 'The sum of sunroom area should be measured as a space bounded by facade surface of the exterior wall and inner side of sunroom fence without taking into account area that is covered by the fence'
    },
    precision: 2,
    hideIfNull: true,
    type: 'num',
    isRequired: true,
    isVisible: true
  },
  totalArea: {
    name: {
      ru: 'Общая площадь (0.00 м²)',
      en: 'Total area (0.00 m²)'
    },
    help: {
      ru: 'Сумма площадей всех помещений квартиры',
      en: 'The sum of all areas of apartment'
    },
    precision: 2,
    limits: {
      studio25: {
        colors: [20, 24, 26, 30],
        target: [25, 25]
      },
      oneBedAp45: {
        colors: [40, 44, 46, 50],
        target: [45, 45]
      },
      oneBedAp65: {
        colors: [60, 64, 66, 70],
        target: [65, 65]
      },
      twoBedAp85: {
        colors: [80, 84, 86, 90],
        target: [85, 85]
      },
      threeBedAp105: {
        colors: [100, 104, 106, 110],
        target: [105, 105]
      },
      studio30: {
        colors: [25, 29, 31, 35],
        target: [30, 30]
      },
      oneBedAp50: {
        colors: [45, 49, 51, 55],
        target: [50, 50]
      },
      oneBedAp70: {
        colors: [65, 69, 71, 75],
        target: [70, 70]
      },
      twoBedAp90: {
        colors: [85, 89, 91, 95],
        target: [90, 90]
      },
      threeBedAp110: {
        colors: [105, 109, 111, 115],
        target: [110, 110]
      }
    },
    type: 'numCalculated',
    isRequired: false,
    isVisible: true
  }
}

const subBlocks = {
  urbanVilla: {
    name: {
      ru: 'Малоквартирный дом',
      en: 'Urban villa'
    },
    marked: false,
    excluded: [teps.sectionLength]
  },
  sectionBuilding: {
    name: {
      ru: 'Секционный дом',
      en: 'Section building'
    },
    marked: false
  },
  galleryBuilding: {
    name: {
      ru: 'Галерейный дом',
      en: 'Gallery building'
    },
    marked: false
  },
  tower: {
    name: {
      ru: 'Башня',
      en: 'Tower'
    },
    marked: false,
    excluded: [teps.sectionLength]
  },
  // flats
  studio25: {
    name: {
      ru: 'Студия (25 м²)',
      en: 'Studio (25 m²)'
    }
  },
  oneBedAp45: {
    name: {
      ru: 'Квартира с 1-ой спальней (45 м²)',
      en: '1-bedroom apartment (45 m²)'
    }
  },
  oneBedAp65: {
    name: {
      ru: 'Квартира с 1-ой спальней (65 м²)',
      en: '1-bedroom apartment (65 m²)'
    }
  },
  twoBedAp85: {
    name: {
      ru: 'Квартира с 2-мя спальнями (85 м²)',
      en: '2-bedroom apartment (85 m²)'
    }
  },
  threeBedAp105: {
    name: {
      ru: 'Квартира с 3-мя спальнями (105 м²)',
      en: '3-bedroom apartment (105 m²)'
    }
  },
  studio30: {
    name: {
      ru: 'Студия (30 м²)',
      en: 'Studio (30 m²)'
    }
  },
  oneBedAp50: {
    name: {
      ru: 'Квартира с 1-ой спальней (50 м²)',
      en: '1-bedroom apartment (50 m²)'
    }
  },
  oneBedAp70: {
    name: {
      ru: 'Квартира с 1-ой спальней (70 м²)',
      en: '1-bedroom apartment (70 m²)'
    }
  },
  twoBedAp90: {
    name: {
      ru: 'Квартира с 2-мя спальнями (90 м²)',
      en: '2-bedroom apartment (90 m²)'
    }
  },
  threeBedAp110: {
    name: {
      ru: 'Квартира с 3-мя спальнями (110 м²)',
      en: '3-bedroom apartment (110 m²)'
    }
  }
}

const blocks = {
  buildings25: {
    name: {
      ru: 'Здания с квартирами среднего размера (25-105 м²)',
      en: 'Buildings with medium-size apartments (25-105 м²)'
    },
    shouldSelected: 3,
    teps: [
      teps.numberOfStoreys,
      teps.numberOfNonResidentialStoreys,
      teps.buildingFootprint,
      teps.netArea,
      teps.grossArea,
      teps.nonResidentialArea,
      teps.collectiveSpaces,
      teps.apartmentsPerFloor,
      teps.apartmentsPerBuilding,
      teps.studioPercent,
      teps.oneBedroomAps45,
      teps.oneBedroomAps65,
      teps.twoBedroomAps85,
      teps.threeBedroomAps105,
      teps.uniqueAps,
      teps.totalApsArea,
      teps.floorHeight,
      teps.ceilingHeight,
      teps.sectionLength,
      teps.activeFacades,
      teps.elevators,
      teps.serviceElevator,
      teps.twoSidedOrientation,
      teps.conciergeRoom,
      teps.transparentEntrance,
      teps.twoStoreyAps,
      teps.storageSpace,
      teps.k1,
      teps.k2
    ],
    vis: [
      vis.groundFloor,
      vis.standardFloor,
      vis.section1,
      vis.section2,
      vis.facade1,
      vis.facade2,
      vis.scheme1,
      vis.catalogue
    ],
    notes: [
      notes.floorArea,
      notes.loweringFactors
    ],
    items: [
      subBlocks.urbanVilla,
      subBlocks.sectionBuilding,
      subBlocks.galleryBuilding,
      subBlocks.tower
    ]
  },
  buildings30: {
    name: {
      ru: 'Здания с квартирами увеличенного размера (30-110м²)',
      en: 'Buildings with large-size apartments (30-110м²)'
    },
    shouldSelected: 3,
    teps: [
      teps.numberOfStoreys,
      teps.numberOfNonResidentialStoreys,
      teps.buildingFootprint,
      teps.netArea,
      teps.grossArea,
      teps.nonResidentialArea,
      teps.collectiveSpaces,
      teps.apartmentsPerFloor,
      teps.apartmentsPerBuilding,
      teps.studioPercent,
      teps.oneBedroomAps45,
      teps.oneBedroomAps65,
      teps.twoBedroomAps85,
      teps.threeBedroomAps105,
      teps.uniqueAps,
      teps.totalApsArea,
      teps.floorHeight,
      teps.ceilingHeight,
      teps.sectionLength,
      teps.activeFacades,
      teps.elevators,
      teps.serviceElevator,
      teps.twoSidedOrientation,
      teps.conciergeRoom,
      teps.transparentEntrance,
      teps.twoStoreyAps,
      teps.storageSpace,
      teps.k1,
      teps.k2
    ],
    notes: [
      notes.floorArea,
      notes.loweringFactors
    ],
    vis: [
      vis.groundFloor,
      vis.standardFloor,
      vis.section1,
      vis.section2,
      vis.facade1,
      vis.facade2,
      vis.scheme1,
      vis.catalogue
    ],
    items: [
      subBlocks.urbanVilla,
      subBlocks.sectionBuilding,
      subBlocks.galleryBuilding,
      subBlocks.tower
    ]
  },
  flats25: {
    name: {
      ru: 'Квартиры среднего размера (25-105 м²)',
      en: 'Medium-size apartments (25-105 м²)'
    },
    teps: [
      teps.livingArea,
      teps.nonLivingArea,
      teps.sunroomsAreaBT,
      teps.sunroomsAreaLoggies,
      teps.totalArea
    ],
    vis: [
      vis.apartmentPlan,
      vis.interior
    ],
    notes: [
      notes.measureARoom,
      notes.measureAnAp,
      notes.typeOfSunroom
    ],
    items: [
      subBlocks.studio25,
      subBlocks.oneBedAp45,
      subBlocks.oneBedAp65,
      subBlocks.twoBedAp85,
      subBlocks.threeBedAp105
    ]
  },
  flats30: {
    name: {
      ru: 'Квартиры увеличенного размера (30-110м²)',
      en: 'Large-size apartments (30-110м²)'
    },
    teps: [
      teps.livingArea,
      teps.nonLivingArea,
      teps.sunroomsAreaBT,
      teps.sunroomsAreaLoggies,
      teps.totalArea
    ],
    vis: [
      vis.apartmentPlan,
      vis.interior
    ],
    notes: [
      notes.measureARoom,
      notes.measureAnAp,
      notes.typeOfSunroom
    ],
    items: [
      subBlocks.studio30,
      subBlocks.oneBedAp50,
      subBlocks.oneBedAp70,
      subBlocks.twoBedAp90,
      subBlocks.threeBedAp110
    ]
  }
}

const projects = [
  {

    desc: {
      ru: '',
      en: ''
    },
    items: [
      blocks.flats25,
      blocks.flats30,
      blocks.buildings25,
      blocks.buildings30
    ],
    vis: [
    ]
  }
]
addKeys(vis)
addKeys(blocks)
addKeys(subBlocks)
addKeys(teps)
feelTepsWithTemplatesData()

function feelTepsWithTemplatesData () {
  _.each(projects, (project) => {
    _.each(project.items, (block) => {
      _.each(block.items, (subBlock) => {
        subBlock.items = _.reject(block.teps, (tep) => {
          return _.some(subBlock.excluded, (e) => e.key === tep.key)
        })
        subBlock.items = _.map(subBlock.items, (tep) => {
          return _.assign({ xx: 'yy' }, _.omit(tep, 'limits'), tep.limits ? { limits: tep.limits[subBlock.key] } : {}, {precision: tep.precision})
        })
        subBlock.vis = block.vis
      })
    })
  })
}

function addKeys (obj) {
  return _.transform(obj, (total, v, k) => {
    v.key = k
    total[k] = v
    return total;
  }, {})
  // _.each(obj, (v, k) => {
  //   v.key = k
  // })
}

exports.projects = projects;
exports.vis = vis;
exports.blocks = blocks;
exports.subBlocks = subBlocks;
exports.teps = teps;

