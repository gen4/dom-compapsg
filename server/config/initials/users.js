module.exports = [
  {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    username: 'admin',
    password: '123'
  },
  {
    provider: 'local',
    role: 'guest',
    name: 'Guest',
    username: 'guest',
    password: '123'
  },
  {
    provider: 'local',
    role: 'voter',
    name: 'Voter',
    username: 'voter',
    password: '123'
  },
  {
    provider: 'local',
    role: 'user',
    name: 'User',
    username: 'user',
    password: '123'
  }
]
