'use strict'

var express = require('express')
var User = require('../../api/user/user.model')
var remoteUser = require('../../helpers/remoteUser')
var router = express.Router()

router.post('/', function (req, res, next) {

  if (!req.body.userToken || !req.body.userId) return res.status(400).json({ error: 'Token is required' })

  remoteUser(req.body)
    .then((newUser) => {
      if (!newUser.id6) {
        return Promise.reject(new Error('Auth error #6'))
      }
      return User.getOrCreateNew(newUser)
        .then((token) => {
          res.json({ token })
        })
    })
    .catch((err) => {
      console.log('ee', err)
      return res.status(400).json({ error: err })
    })
    // var token = auth.signToken(user)
    // res.json({ token: token })
  // })(req, res, next)
})

module.exports = router
