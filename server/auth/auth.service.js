'use strict'

// var mongoose = require('mongoose')
// var passport = require('passport')
var config = require('../../config').backend
var _ = require('lodash')
var jwt = require('jsonwebtoken')
var expressJwt = require('express-jwt')
var compose = require('composable-middleware')
var User = require('../api/user/user.model')
var Vis = require('../api/vis/vis.model')
var validateJwt = expressJwt({ secret: config.secrets.session })

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated () {
  return compose()
    // Validate jwt
    .use(function (req, res, next) {
      // allow access_token to be passed through query parameter as well
      if (req.query && req.query.hasOwnProperty('access_token')) {
        req.headers.authorization = 'Bearer ' + req.query.access_token // eslint-disable-line
      }
      validateJwt(req, res, next)
    })
    // Attach user to request
    .use(function (req, res, next) {
      User.findById(req.user._id, function (err, user) {
        if (err) return next(err)
        if (!user) return res.sendStatus(401)

        req.user = user
        next()
      })
    })
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
function hasRole (roleRequired) {
  if (!roleRequired) throw new Error('Required role needs to be set')
  if (typeof roleRequired === 'string') {
    if (config.userRoles.indexOf(roleRequired) < 0) throw new Error(`Wrong role [${roleRequired}]`)
  } else {
    if (_.every(roleRequired, (rr) => config.userRoles.indexOf(rr) < 0)) throw new Error(`Wrong role [${roleRequired}]`)
  }
  return compose()
    .use(isAuthenticated())
    .use(function meetsRequirements (req, res, next) {
      console.log(req.user.role, roleRequired)
      // config.userRoles.indexOf(req.user.role) >= 0 && config.userRoles.indexOf(roleRequired)
      if (typeof roleRequired === 'string') {
        if (req.user.role === roleRequired) {
          next()
        } else {
          res.sendStatus(403)
        }
      } else {
        if (_.indexOf(roleRequired, req.user.role) >= 0) {
          next()
        } else {
          res.sendStatus(403)
        }
      }
    })
}

function onlyVisOwner (req, res, next) {
  if (req.user && req.user.role === 'admin') {
    return next()
  }

  Vis.findOne({ _id: req.params.id, user: req.user._id })
    .select('_id')
    .lean()
    .then((vis) => {
      if (!vis) {
        return res.sendStatus(400)
      }
      return next()
    })
}

/**
 * Returns a jwt token signed by the app secret
 */
function signToken (user) {
  return jwt.sign({ _id: user._id, name: user.name, role: user.role }, config.secrets.session, { expiresIn: '7d' })
}

/**
 * Set token cookie directly for oAuth strategies
 */
function setTokenCookie (req, res) {
  if (!req.user) return res.json(404, { message: 'Something went wrong, please try again.' })
  var token = signToken(req.user)
  res.cookie('token', JSON.stringify(token))
  res.redirect('/')
}

exports.isAuthenticated = isAuthenticated
exports.hasRole = hasRole
exports.signToken = signToken
exports.setTokenCookie = setTokenCookie
exports.onlyVisOwner = onlyVisOwner
