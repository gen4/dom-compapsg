const Upload = require('../api/uploads/upload.model')
const Project = require('../api/project/project.model')
var config = require('../../config').backend
const _ = require('lodash')
const Promise = require('bluebird')
const fs = require('fs')
const path = require('path')
var createDirSync = require('../helpers/filesystem')
var AWS = require('aws-sdk')
var ctrl = require('./controller')
// var https = require('https')
// var agent = new https.Agent({
//   rejectUnauthorized: true,
//   maxSockets: 1000 // default
// });
AWS.config.update({
  region: 'ru-msk',
  endpoint: process.env.S3_HOST || 'https://hb.bizmrg.com'
})

var s3 = new AWS.S3()
  // {
  // httpOptions: {
  //   timeout: 3000,
  //   agent: agent
  // }
// })
const s3PathStart = `${process.env.S3_BUCKET}.${process.env.S3_HOST.split('://')[1]}`
function getFile (id6, url, keyPath) {
  console.log('get file', url, keyPath, process.env.S3_BUCKET, process.env.S3_HOST)
  console.log(config.uploadDir, id6, keyPath)

  const pathArr = keyPath.split('/')
  const fileName = pathArr.splice(-1, 1)[0]
  const filePath = _.map(pathArr, (p) => `${id6}_${p}`).join('/')
  const fileExt = _.takeRight(url.split('.'))
  const publicPath = `${id6}_dl/${filePath}/${id6}_${fileName}.${fileExt}`
  const pathToSave = `${createDirSync(path.resolve(config.uploadDir, `${id6}_dl`, filePath))}${id6}_${fileName}.${fileExt}`
  console.log('\t', filePath, fileName, pathToSave, ':::', publicPath)
  var objKey = _.trimStart(url.split(s3PathStart)[1], '/')
  // console.log('objKey', pathToSave, ":", s3PathStart, objKey)

  var params = { Bucket: process.env.S3_BUCKET, Key: objKey }
  var file = fs.createWriteStream(pathToSave)

  return new Promise((resolve, reject) => {

    file.on('close', () => {
      return resolve(publicPath)
    })
    const s3Promise = s3.getObject(params).promise()

    s3Promise.then((data) => {
      file.write(data.Body, () => {
        file.end()
        return resolve(publicPath)
      });
    }).catch((err) => {
      console.error(err)
      return reject(err)
    })
      // s3.getObject(params)
      //   .createReadStream()
      //   .on('end', () => {
      //     console.log('-3')
      //     return resolve(publicPath)
      //   })
      //   .on('error', (error) => {
      //     console.log('-3e')
      //     console.error(error)
      //     return reject(error)
      //   })
      //   .pipe(file)
      //   .on('error', (error) => {
      //     console.log('-3ep')
      //     console.error(error)
      //     return reject(error)
      //   })
    // } catch (err) {
    //   console.log('-3e+')
    //   console.error(err)
    //   return reject(err)
    // }
  })
}
// function downloadOne(uploadId) {
//   const path = '/'
//   return Upload.findById(uploadId)
//     .lean()
//     .then((u) => {
//       getFile(u.s3Url, path)
//     })
//   // getUpload
//   // markProject
//   // fire next
// }

function getNextUpload () {
 // console.log('Check next upload')
  // TODO chanche state
  Upload.findOneAndUpdate({ state: 'scheduled' }, { state: 'inTransfer' })
    .select('+project')
    .populate({
      path: 'project',
      select: '_id user',
      populate: {
        path: 'user',
        select: 'id6'
      }
    })
    .then((u) => {
      if (!u) {
        return setTimeout(getNextUpload, 4000)
      }
      if (!u.project || !u.project.user || !u.project.user.id6) {
        return Upload.markAsErrored(u._id, 'no id6 found')
          .then(() => {
            return ctrl.updateProjectDownloads(u.project._id)
          })
          .then(() => {
            getNextUpload(u.project._id)
            return null
          })
      }
      return getFile(String(u.project.user.id6), u.s3Url || u.url, u.keyPath)
        .then((publicPath) => {
          console.log('update upload')
          return Upload.findByIdAndUpdate(u, { state: 'transferred', url: publicPath, localUrl: publicPath })
        })
        .then(() => {
          console.log('update project')
          return ctrl.updateProjectDownloads(u.project._id)
        })
        .then(() => {
          getNextUpload()
          return null
        })
        .catch((err) => {
          console.error(err)
          return Upload.markAsErrored(u._id, String(err))
            .then(() => {
              return ctrl.updateProjectDownloads(u.project._id)
            })
            .then(() => {
              getNextUpload()
            })
        })
    })
}
//
// function updateProjectDownloads (pid) {
//   return Promise.all([
//     Upload.count({ project: pid, keyPath: { $exists: true }}),
//     Upload.count({ project: pid, keyPath: { $exists: true }, state: 'transferred' }),
//     Upload.count({ project: pid, keyPath: { $exists: true }, state: 'error' })
//   ])
//     .spread((total, done, errs) => {
//       console.log('U P ', pid, total, done , errs);
//       return Project.findByIdAndUpdate(pid, {
//         download: {
//           total,
//           done,
//           errs
//         }
//       })
//     })
// }

getNextUpload()
