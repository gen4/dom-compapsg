/**
 * Main application file
 */

'use strict'
require('dotenv').config()
// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

var express = require('express')
var mongoose = require('mongoose')
mongoose.Promise = require('bluebird')
var config = require('../../config').backend


// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options)

// Setup server
var app = express()
var server = require('http').createServer(app)
require('../config/express')(app)
require('./routes')(app)
require('./worker')
// const smb = require('./smb')
// smb.transferToSmb('019547')

// Start server
server.listen(config.downloaderPort, config.ip, function () {
  console.log('Downloader server listening on %d, in %s mode', config.downloaderPort, app.get('env'))
})

// Expose app
module.exports = app
