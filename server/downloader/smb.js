const path = require('path')
var SMB2 = require('smb2');
var config = require('../../config').backend
var fs = require('fs')
var Promise = require('bluebird')

// create an SMB2 instance
var smb2Client = new SMB2({
  share:'\\\\000.000.000.000\\c$'
  , domain:'DOMAIN'
  , username:'username'
  , password:'password!'
});

function transferFile (localPath, remotePath) {
  console.log('transferFile', localPath, '>', remotePath)
  return Promise.resolve()
}

function walk (dir, remoteDir, results) {
  return Promise.resolve()
    .then(() => {
      return new Promise((resolve, reject) => {
        fs.readdir(dir, function (err, list) {
          if (err) return reject(err);
          return Promise.each(list, (item) => {
            if (!item) return resolve(results)
            const file = path.resolve(dir, item)
            return new Promise((resolve2, reject2) => {
              fs.stat(file, (errstat, stat) => {
                if (errstat) return reject2(errstat);
                if (stat && stat.isDirectory()) {
                  return walk(file, `${remoteDir}/${item}`, results)
                    .then(() => {
                      return resolve2(results)
                    })
                } else {
                  return transferFile(file, `${remoteDir}/${item}`)
                    .then(() => resolve2(results))
                }
              })
            })
          })
            .then(() => {
              resolve(results)
            })
        })
      })
    })
}

exports.transferToSmb = function (id6) {
  console.log('saving smb')
  const dlpath = path.resolve(config.uploadDir, `${id6}_dl`)
  const csvpath = path.resolve(config.uploadDir, `${id6}_csv`)
  // todo getAllFiles
  // todo
  // smb2Client.writeFile('path\\to\\my\\file.txt', 'Hello Node', function (err) {
  //   if (err) throw err
  //   console.log('It\'s saved!')
  // })
  walk(dlpath, `${id6}_dl`, [])
    .then((files) => {
      console.log('f', files);
    })
    .catch((err) => {
      console.error(err)
    })
}

exports.transferFromSmb = function (pid) {

}
