const _ = require('lodash')
const Promise = require('bluebird')
const Project = require('../api/project/project.model')
const Stage = require('../api/stage/stage.model')
const Like = require('../api/like/like.model')
const Vis = require('../api/vis/vis.model')
const User = require('../api/user/user.model')
const Upload = require('../api/uploads/upload.model')
const projectInitials = require('../config/initials/projects').projects

const csvStringify = require('csv-stringify')
const csvParse = require('csv-parse')
const fs = require('fs')
const path = require('path')
const { saveToCSV, saveToCSVOne } = require('./csv')
var config = require('../../config').backend

const visFields = ['album', 'panel1', 'panel2', 'albumPrint', 'panel1Print', 'panel2Print', 'schemas', 'decl', 'consent', 'note', 'blueprint']

function getFileEndName (i) {
  return i ? `_${i}` : ''
}

function markAllProjectVis4Download (project) {
  const allVis = []

  _.each(project.visConverted, (v, i) => {
    v.keyPath = `vis${getFileEndName(i)}`
    allVis.push(v)
  })

  _.each(visFields, (vf) => {
    project[vf].keyPath = vf
    allVis.push(project[vf])
  })

  _.each(project.items, (block) => {
    _.each(block.items, (subBlock) => {
      _.each(subBlock.vis, (v) => {
        v.keyPath = `${block.key}/${subBlock.key}/${v.key}`
        allVis.push(v)
      })
    })
  })

  const allUploads = []
  _.each(allVis, (v) => {
    _.each(v.uploads, (u, i) => {
      u.keyPath = `${v.keyPath}${getFileEndName(i)}`
      allUploads.push(u)
    })
  })
  // console.log('vis', JSON.stringify(allUploads, null, 2))
  return Promise.map(allUploads, (u) => Upload.markAsScheduledForDownload(u._id, u.keyPath), { concurrency: 10 })
}

function updateProjectDownloads (pid) {
  return Promise.all([
    Upload.count({ project: pid, keyPath: { $exists: true }}),
    Upload.count({ project: pid, keyPath: { $exists: true }, state: 'transferred' }),
    Upload.count({ project: pid, keyPath: { $exists: true }, state: 'error' })
  ])
    .spread((total, done, errs) => {
      return Project.findByIdAndUpdate(pid, {
        download: {
          total,
          done,
          errs
        }
      })
    })
}

function scheduleNewDownload (projectId) {
  return Project.getFullAdmin(projectId)
    .then((project) => {
      return markAllProjectVis4Download(project)
    })
    .then(() => {
      return updateProjectDownloads(projectId)
    })
}


function csv(projectId) {
  return Project.getFullAdmin(projectId)
    .then((project) => {
      const things2save = {}
      _.each(project.items, (block) => {
        _.each(block.items, (subBlock) => {
          things2save[`${project.user.id6}_csv_${block.key}_${subBlock.key}`] = []
          const trg = things2save[`${project.user.id6}_csv_${block.key}_${subBlock.key}`]
          _.each(subBlock.items, (tep) => {
            trg.push({
              name: `${tep.name.en}`, // ${tep.name.ru}
              value: tep.value
            })
          })
        })
      })
      return saveToCSV(project.user.id6, things2save)
    })
}

exports.album = function (projectId) {
  return Project.convertAlbum(projectId)
}
exports.allProjectsLikesCsv = function (stageId) {

  // return Stage.findById(stageId)
  //   .lean()
  //   .then((stage) => {
  //   return Like.aggregate([
  //     { $match: { stage: stageId}},
  //     { $lookup: { from: 'users', localField: 'user', foreignField: '_id', as: 'login' }},
  //     { $lookup: { from: 'projects', localField: 'project', foreignField: '_id', as: 'work' }},
  //     { $project: {
  //       _id: 0,
  //       username: {
  //         $arrayElemAt: ['$login.username', 0]
  //       },
  //       value: 1,
  //       like: { $cond: [{ $gt: ['$value', 0] }, 1, 0] },
  //       dislike: { $cond: [{ $lte: ['$value', 0] }, 1, 0] }
  //     }},
  //     {
  //       $group: {
  //         _id: '$username',
  //         likes: { $sum: '$like' },
  //         dislikes: { $sum: '$dislike' },
  //         total: { $sum: 1 }
  //       }
  //     }
  //   ])
  // })
  var currentStage = null
  // console.log('t1')
  return Stage.getCurrent()
    .then((prevStage) => {
      console.log('t1112', prevStage)
      currentStage = prevStage
  })
    .then(() => {
      return Project
        .find({ isSubmitted: true })
        .select('_id id4 id6 isSubmitted')
        .lean()
    })
    .then((projects) => {
      console.log('t2', projects.length)
      return _.map(projects, (project) => {
        return { project: project, score: 0 }
      })
    })
    .then((projects) => {
      console.log('z')
      console.log('z2', `=${stageId}=`)
      return likesByProjects(stageId)
        .then((likes) => {
          console.log('t3', likes.length)
          return _.map(projects, (project) => {
            var like = _.find(likes, { _id: project.project._id })
            project = _.assign(project, _.omit(like, '_id'))
            return {
              id4: project.id4,
              id6: project.id6,
              isSelected: _.indexOf(currentStage.passedWorks, project._id) >= 0,
              likes: like.score
            }
          })
        })
    })
    .then((res) => {
      console.log('t9', res)
      return saveToCSVOne('_allLikes', res)
    })
}
function likesByProjects (stageId) {
  return Like.aggregate([{
    $group: {
      _id: '$project',
      likes: { '$sum': 1 },
      score: { '$sum': '$value' }
    }
  }
  ])
}

exports.allProjectsCsv = function () {
  return Project.find({})
    .select('user readiness isSubmitted isApproved desc')
    .populate({
      path: 'user',
      select: '+id4 +id6'
    })
    .then((projects) => {
      const cont = _.map(projects, (p) => {
        return {
          id4: String(p.user.id4),
          id6: `=${String(p.user.id6)}`,
          email: p.user.email,
          descRU: p.desc.ru,
          descEN: p.desc.en,
          readiness: p.readiness,
          isSubmitted: p.isSubmitted,
          isApproved: p.isApproved
        }
      })
      return new Promise((resolve, reject) => {
        const filepath = `${config.uploadDir}/allprojects.csv`
        csvStringify(cont, {
          // columns: _.keys(cont),
          header: true,
          quoted: true,
          quotedString: true,
        }, (err, str) => {
          fs.writeFile(filepath, str, 'utf8', function (err) {
            if (err) {
              console.log('Some error occured - file either not saved or corrupted file saved.')
              return reject(err)
            } else {
              console.log('It\'s saved!')
              return resolve(filepath)
            }
          })
        })
      })
  })
}
exports.totalFetch = function () {
  return Promise.resolve()
    .then(() => {
      return Project.find({})
        .select('_id')
        .lean()
    })
    .then((allProjectsIds) => {
      return Promise.resolve()
        .then(() => {
          let i = 0
          const now = new Date().getTime() - 1000000;
          return Promise.each(allProjectsIds, (pid) => {
            console.log(`revoking deadlines [${i++} / ${allProjectsIds.length}] \t [${pid._id}]`)
            return Project.findByIdAndUpdate(pid, { editionAllowedTill: now })
          })
        })
        .then(() => {
          let i = 0
          return Promise.each(allProjectsIds, (pid) => {
            console.log(`Downloading [${i++} / ${allProjectsIds.length}]  \t [${pid._id}]`)
            return scheduleNewDownload(pid)
          })
        })
        .then(() => {
          let i = 0
          return Promise.each(allProjectsIds, (pid) => {
            console.log(`CSVs [${i++} / ${allProjectsIds.length}]  \t [${pid._id}]`)
            return csv(pid)
          })
        })
    })
}

exports.genAllCSVs = function () {
  return Promise.resolve()
    .then(() => {
      return Project.find({})
        .select('_id')
        .lean()
    })
    .then((allProjectsIds) => {
      let i = 0
      return Promise.each(allProjectsIds, (pid) => {
        console.log(`CSVs [${i++} / ${allProjectsIds.length}]  \t [${pid._id}]`)
        return csv(pid)
      })
    })
}

function totalConverts () {
  let i = 0
  return Project.find({
    isSubmitted: true,
    $or: [
          { visConverted: { $exists: false }},
          { albumConverted: { $exists: false }}
    ]
  })
    .then((allProjects) => {
      console.log('All projects', allProjects.length)
      return Promise.each(allProjects, (p) => {
        const tasks = []
        if (!p.visConverted || p.visConverted.length <= 0) {
          tasks.push(Project.convertAllVis(p._id))
        }
        // if (!p.albumConverted) {
        //   tasks.push(Project.convertAlbum(p._id))
        // }
        console.log('tasks ', tasks.length, p.visConverted)
        return new Promise((resolve, reject) => {
          return Promise.all(tasks)
            .then(() => {
              console.log('totalConverts: ', i++, allProjects.length)
              return resolve()
            })
            .catch(() => {
              return resolve()
            })
        })
      })
    })
}

function totalPanelsConverts () {
  let i = 0
  return Project.find({
    isSubmitted: true
  })
    .then((allProjects) => {
      console.log('All projects', allProjects.length)
      return Promise.each(allProjects, (p) => {
        const tasks = []
        tasks.push(Project.convertAllPanels(p._id))
        return new Promise((resolve, reject) => {
          return Promise.all(tasks)
            .then(() => {
              console.log('totalConverts: ', i++, allProjects.length)
              return resolve()
            })
            .catch(() => {
              return resolve()
            })
        })
      })
    })
}

function totalConvertsPers (pid) {
  return Promise.all([
    Project.convertAllPanels(pid),
    Project.convertAlbum(pid)
  ])
}

function fixUrl (url, pid) {
  const a = url.split(pid)
  const f = _.slice(a, 0, -1)
  const e = _.slice(a, -1)
  return `${f.join(pid)}/${pid}${e}`
}

function picfix (pid) {
  return Project.getFullAdmin(pid)
    .then((project) => {
      return Promise.each(project.items, (block) => {
        return Promise.each(block.items, (subblock) => {
          return Promise.each(subblock.vis, (vis) => {
            return Promise.each(vis.uploads, (upload) => {
              console.log('\nURL', upload.url)
              console.log('Fixed', fixUrl(upload.url, project.user.id6))
              return Upload.findByIdAndUpdate(upload._id, { url: fixUrl(upload.url, project.user.id6) })
            })
          })
        })
      })
    })
}

function picfixAll (pid) {
  return Project.find({ isSubmitted: true })
    .select('_id')
    .lean()
    .then((projects) => {
      return Promise.map(projects, (p) => picfix(p._id))
    })
}

function id4transfer () {
  return Project.find({})
    .select('id4 id6 _id')
    .populate({
      path: 'user',
      select: 'id4'
    })
    .then((projects) => {
      return Promise.each(projects, (p) => {
        if(p && p.user){
          console.log(' ID4TRANSFER: ', p._id, p.id6, p.user.id4)
          return Project.findByIdAndUpdate(p._id, { id4: p.user.id4 })
        }
        return null
      })
    })
}

function setPrices () {
  const prices = require('./prices.json')
  const values = [null, 'l', 'm', 'h']
  return Promise.each(_.map(prices, (val, id) => ({ id: id, val: val })), (item) => {
    console.log(item.id, item.val, values[item.val])
    return Project.findOneAndUpdate({ id6: item.id }, { editorPrice: values[item.val] })
  })
}

function readCSV(filename){
  return new Promise((resolve, reject) => {

    fs.readFile(path.resolve(__dirname, filename),  'utf8',(err, file) => {
      if(err){
        return reject(err)
      }
      csvParse(file, {delimiter: ';'}, function(err, output){
        if (err) {
          return reject(err)
        }
        return resolve(output)
      })

    })
  })

}

function setComplience () {
  const values = [null, 'h', 'm', 'l']
  return Promise.all([
    readCSV('./gd_csv_buildings.csv'),
    readCSV('./gd_csv_flats.csv')
  ])
    .spread((b, f) => {
      return Promise.each(b, (item) => {
        const id6 = item[0].length === 5 ? `0${item[0]}` : item[0]
        console.log('b', id6, values[item[1]])
        return Project.findOneAndUpdate({ id6: id6 }, { editorTzBuilding: values[item[1]] })
      }).then(() => {
        return Promise.each(f, (item) => {
          const id6 = item[0].length === 5 ? `0${item[0]}` : item[0]
          console.log('f', id6, values[item[1]])
          return Project.findOneAndUpdate({ id6: id6 }, { editorTzFlat: values[item[1]] })
        })
      })
      // console.log('\nb',b)
      // console.log('\nf',f)
      // return null
    })
  // const prices = require('./prices.json')
  // const values = [null, 'l', 'm', 'h']
  // return Promise.each(_.map(prices, (val, id) => ({ id: id, val: val })), (item) => {
  //   console.log(item.id, item.val, values[item.val])
  //   return Project.findOneAndUpdate({ id6: item.id }, { editorPrice: values[item.val] })
  // })
}


function findMutants () {
  const pi = projectInitials[0]
  const mutants = {}
  return Project.find({isSubmitted: true})
    .select('_id')
    .then((projects) => {
      return Promise.each(projects, (p) => {
        return Project.getFullAdmin(p._id)
          .then((p) => {
            const isOk = _.every(p.items, (block, bIndex) => {
              // console.log(pi.items)
              const iblock = _.find(pi.items, { key: block.key })
              return _.every(block.items, (sb, sbIndex) => {
                const isb = _.find(iblock.items, { key: sb.key })
                return _.each(sb.items, (tep) => {
                  const isTepPresentInInitials = _.find(isb.items, { key: tep.key })
                  if (!isTepPresentInInitials) {
                    if (!mutants[p.id6]) {
                      mutants[p.id6] = {
                        id6: p.id6,
                        id4: p.id4,
                        id: p._id,
                        teps2remove: [],
                        teps2add: [],
                        vis2add: [],
                        vis2remove: []
                      }
                    }
                    mutants[p.id6].teps2remove.push({
                      key: tep.key,
                      id: tep.id,
                      mongoKey: tep.mongoKey,
                      b: block.key,
                      sb: sb.key
                    })
                  }
                  return null
                }) && _.each(isb.items, (it) => {
                  const isInitialPresentInTep = _.find(sb.items, { key: it.key })
                  if (!isInitialPresentInTep) {
                    if (!mutants[p.id6]) {
                      mutants[p.id6] = {
                        id6: p.id6,
                        id4: p.id4,
                        id: p._id,
                        teps2remove: [],
                        teps2add: [],
                        vis2add: [],
                        vis2remove: []
                      }
                    }
                    mutants[p.id6].teps2add.push({
                      key: it.key,
                      item: it,
                      mongoKey: `items.${bIndex}.items.${sbIndex}.items`,
                      b: block.key,
                      sb: sb.key
                    })
                  }
                  return null
                }) && _.each(sb.vis, (vis) => {
                  const isVisPresentInInitials = _.find(isb.vis, { key: vis.key })
                  if (!isVisPresentInInitials) {
                    if (!mutants[p.id6]) {
                      mutants[p.id6] = {
                        id6: p.id6,
                        id4: p.id4,
                        id: p._id,
                        teps2remove: [],
                        teps2add: [],
                        vis2add: [],
                        vis2remove: []
                      }
                    }
                    mutants[p.id6].vis2remove.push({
                      key: vis.key,
                      mongoKey: vis.mongoKey,
                      id: vis._id,
                      b: block.key,
                      sb: sb.key
                    })
                  }
                  return null
                }) && _.each(isb.vis, (iv) => {
                  const isInitialPresentInVis = _.find(sb.vis, { key: iv.key })
                  if (!isInitialPresentInVis) {
                    if (!mutants[p.id6]) {
                      mutants[p.id6] = {
                        id6: p.id6,
                        id4: p.id4,
                        id: p._id,
                        teps2remove: [],
                        teps2add: [],
                        vis2add: [],
                        vis2remove: []
                      }
                    }
                    mutants[p.id6].vis2add.push({
                      key: iv.key,
                      item: iv,
                      mongoKey: `items.${bIndex}.items.${sbIndex}.items`,
                      b: block.key,
                      sb: sb.key
                    })
                  }
                  return null
                })
              })
            })

            // if (!isOk) {
              // mutants.push(p.id6)
              // console.log(p.id6, p._id, ' > ', isOk)
            // }
            return null
          })
      })
    })
    .then(() => {
      const hiddenFields = ['user', 'isSubmitted', 'isApproved', 'peopleFav', 'editorFav', 'editorPrice', 'editorTzFlat', 'editorTzBuilding', 'readiness', 'readinessAdmin', 'download', 'csv', 'id4', 'id6', 'oldUser']
      const hiddenFieldsPlus = _.reduce(hiddenFields, (t, f) => t + `+${f} `, '')
      return Promise.each(_.values(mutants), (mutant) => {
        return Project.findById(mutant.id)
          .select(hiddenFieldsPlus)
          .populate([
            {
              path: 'user',
              options: { lean: true }
            }, {
              path: 'oldUser',
              options: { lean: true }
            }])
          .lean()
          .then((project) => {
            return Project.findByIdAndUpdate(project._id, { user: null, oldUser: project.user, id6: `_${project.id6}`, isSubmitted: false })
              .then(() => project)
          })
          .then((project) => {
            return Project.createEmptyWithUser(project.user || project.oldUser)
              .then((newProject) => {
                return Project.findById(newProject._id)
                  .select('+user +id4 +id6')
                  .lean()
              })
              .then((newProject) => {
                console.log(`TR [${project._id}] [${project.id6}]  ${project.id4} -> ${newProject.id4}`)
                _.each(['desc', 'isSubmitted', 'editorPrice', 'editorTzFlat', 'editorTzBuilding', 'editorFav', 'peopleFav'], (field) => {
                  newProject[field] = project[field]
                })
                _.each(newProject.items, (block) => {
                  const ob = _.find(project.items, { key: block.key })
                  if (!ob) {
                    return
                  }
                  _.each(block.items, (sb) => {
                    const osb = _.find(ob.items, { key: sb.key })
                    if (!osb) {
                      return
                    }
                    _.each(sb.items, (tep) => {
                      const ot = _.find(osb.items, { key: tep.key })
                      if (!ot) {
                        return
                      }
                      Object.assign(tep, _.pick(ot, ['value']))
                    })
                  })
                })
                return Project.findByIdAndUpdate(newProject._id, newProject)
                  .then(() => {
                    return Promise.each(['album', 'albumConverted', 'panel1', 'panel2', 'albumPrint', 'panel1Print', 'panel2Print', 'cover', 'schemas', 'decl', 'consent', 'note', 'blueprint'], (field) => {
                      return Vis.findById(project[field])
                        .lean()
                        .then((oldVis) => {
                          if (newProject[field]) {
                            return Vis.findByIdAndUpdate(newProject[field], { uploads: oldVis.uploads })
                          }
                          const nv = new Vis({
                            uploads: oldVis.uploads,
                            key: oldVis.key,
                            user: oldVis.user,
                            project: newProject._id
                          })
                          return nv.save()
                            .then((savedVis) => {
                              return Project.findByIdAndUpdate(newProject._id, { [field]: savedVis })
                            })
                        })
                    })
                  })
                  .then(() => {
                    return Promise.each(['vis', 'visConverted'], (field) => {
                      return Promise.map(project[field], (oldVisId) => {
                        return Vis.findById(oldVisId)
                          .lean()
                          .then((oldVis) => {
                            if (oldVis) {
                              const nv = new Vis(_.assign({
                                user: oldVis.user,
                                project: newProject._id
                              }, _.pick(oldVis, ['name', 'uploads'])))
                              return nv.save()
                            }
                          })
                      })
                        .then((newVis) => {
                          return Project.findByIdAndUpdate(newProject._id, { [field]: newVis })
                        })
                    })
                  })
                  .then(() => {
                    const tasks = []
                    return Promise.all([
                      Project.getFullAdmin(project._id),
                      Project.getFullAdmin(newProject._id)
                    ])
                      .spread((fullOldProject, fullNewProejct) => {
                        _.each(fullNewProejct.items, (block) => {
                          const ob = _.find(fullOldProject.items, { key: block.key })
                          if (!ob) {
                            return
                          }
                          _.each(block.items, (sb) => {
                            const osb = _.find(ob.items, { key: sb.key })
                            if (!osb) {
                              return
                            }
                            _.each(sb.vis, (vis) => {
                              const ov = _.find(osb.vis, { key: vis.key })
                              if (!ov) {
                                return
                              }
                              tasks.push(
                                  Vis.findById(ov)
                                    .lean()
                                    .then((oldVis) => {
                                      if (oldVis) {
                                        return Vis.findByIdAndUpdate(vis, _.pick(oldVis, ['name', 'uploads']))
                                      }
                                      return null
                                    })
                              )
                            })
                          })
                        })
                        return Promise.all(tasks)
                      })
                  })
              })
            // _.each(mutant.teps2remove, (t2r) => {
            //   const mk = t2r.mongoKey.split('.')
            //   project.items[mk[1]].items[mk[3]].items = _.reject(project.items[mk[1]].items[mk[3]].items, { key: t2r.key })
            // })
            // _.each(mutant.teps2add, (t2a) => {
            //   const mk = t2a.mongoKey.split('.')
            //   project.items[mk[1]].items[mk[3]].items.push(t2a.item)
            // })
            // function addVis() {
            //   return Promise.each(mutant.vis2add, (v2a) => {
            //     return ModelClass.addNewVis(project._id, project.user, `items.${bIndex}.items.${sIndex}.vis`)
            //   })
            // }
            // function removeVis() {
            //
            // }
            // return Promise.all([
            //   Project.findByIdAndUpdate(project._id, project),
            //   addVis,
            //   removeVis
            //   ])
          })
      })
    })
    .then(() => {
      return mutants
    })
    .catch((err) => {
      console.error(err)
    })
}

function convertFewAlbums () {
  const id4s = ['6895', '4039', '9694', '4977', '5200', '5200', '1726', '3870',
    '1266', '3211', '4237', '4193', '9731', '2005', '5810', '1328', '4545',
    '5365', '4654', '5847', '9627', '6534', '6133', '5473', '7365', '8335', '1511', '9362', '3191']
  return Project.find({ id4: { $in: id4s }})
    .select('_id')
    .lean()
    .then((projects) => {
      return Promise.each(projects, (project) => {
        return Project.convertAlbum(project._id)
      })
    })
}

exports.convertFewAlbums = convertFewAlbums
exports.setComplience = setComplience
exports.findMutants = findMutants
exports.setPrices = setPrices
exports.id4transfer = id4transfer
exports.picfix = picfix
exports.picfixAll = picfixAll
exports.csv = csv
exports.totalConvertsPers = totalConvertsPers
exports.scheduleNewDownload = scheduleNewDownload
exports.updateProjectDownloads = updateProjectDownloads
exports.totalConverts = totalConverts
exports.totalPanelsConverts = totalPanelsConverts

// exports.scheduleNewDownload = function (projectId) {
//   return Vis.find({ project: projectId })
//     .select('project uploads')
//     .lean()
//     .then((vis) => {
//       return _.flatten(_.map(vis, (v) => v.uploads))
//     })
//     .then((uploads) => {
//       console.log('uploads', uploads)
//       return Promise.map(uploads, (u) => Upload.markAsScheduledForDownload(u), { concurrency: 10 })
//     })
//     .then((uploadsToDownload) => {
//       console.log('\n\nuploadsToDownload', uploadsToDownload.length, uploadsToDownload)
//       const u4d = _.compact(uploadsToDownload)
//       console.log('\n\nu4d', u4d.length, u4d)
//     })
// }

