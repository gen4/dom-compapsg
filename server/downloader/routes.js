/**
 * Main application routes
 */

'use strict'

var errors = require('../components/errors')
var ctrl = require('./controller')
var handleErrorAsync = require('../api/errorhandler')

module.exports = function (app) {
  // Insert routes below
  // todo sequrity check
  app.post('/startDownload/:projectId', (req, res) => {
    const { projectId } = req.params
    ctrl.scheduleNewDownload(projectId)
      .then((result) => {
        console.log(result)
        res.json({ ok: true })
      })
      .catch(handleErrorAsync(res))
  })
  app.post('/csv/:projectId', (req, res) => {
    const { projectId } = req.params
    ctrl.csv(projectId)
      .then((result) => {
        console.log(result)
        res.json({ ok: true })
      })
      .catch(handleErrorAsync(res))
  })
  app.post('/album/:projectId', (req, res) => {
    const { projectId } = req.params
    ctrl.album(projectId)
      .then((result) => {
        console.log(result)
        res.json({ ok: true })
      })
      .catch(handleErrorAsync(res))
  })
  app.post('/allProjectsCsv', (req, res) => {
    ctrl.allProjectsCsv()
      .then((result) => {
        console.log(result)
        res.json({ filename: result })
      })
      .catch(handleErrorAsync(res))
  })
  app.post('/totalFetch', (req, res) => {
    ctrl.totalFetch()
      .then((result) => {
        console.log(result)
        res.json({ filename: result })
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/genAllCSVs', (req, res) => {
    ctrl.genAllCSVs()
      .then((result) => {
        console.log(result)
        res.json({ filename: result })
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/totalConverts', (req, res) => {
    ctrl.totalConverts()
      .then((result) => {
        // console.log(result)
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/totalConverts/:projectId', (req, res) => {
    ctrl.totalConvertsPers(req.params.projectId)
      .then((result) => {
        console.log(result)
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/picfix/:projectId', (req, res) => {
    ctrl.picfix(req.params.projectId)
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })
  app.post('/picfix', (req, res) => {
    ctrl.picfixAll(req.params.projectId)
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })
  app.post('/id4transfer', (req, res) => {
    ctrl.id4transfer(req.params.projectId)
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/setPrices', (req, res) => {
    ctrl.setPrices()
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/setComplience', (req, res) => {
    ctrl.setComplience()
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/findMutants', (req, res) => {
    ctrl.findMutants()
      .then((result) => {
        res.json(result)
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/convertFewAlbums', (req, res) => {
    ctrl.convertFewAlbums()
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/allProjectsLikesCsv/:stageId', (req, res) => {
    const { stageId } = req.params
    ctrl.allProjectsLikesCsv(stageId)
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.post('/totalPanelsConverts', (req, res) => {
    ctrl.totalPanelsConverts()
      .then((result) => {
        res.json({})
      })
      .catch(handleErrorAsync(res))
  })

  app.route('/:url(api|auth|static)/*').get(errors[404])
}
