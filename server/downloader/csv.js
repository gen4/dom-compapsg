const createDirSync = require('../helpers/filesystem')
const _ = require('lodash')
const fs = require('fs')
const csvStringify = require('csv-stringify');
const path = require('path')
var config = require('../../config').backend

exports.saveToCSV = function(id6, thing2save) {
  _.each(thing2save, (cont, filename) => {
    const pathToSave = `${createDirSync(path.resolve(config.uploadDir, `csv/${id6}_csv`))}${filename}.csv`
    csvStringify(cont, {}, function (err, str) {
      fs.writeFile(pathToSave, str, 'utf8', function (err) {
        if (err) {
          console.log('Some error occured - file either not saved or corrupted file saved.');
        } else {
          return null
        }
      });
    })
  })
  return null
}

exports.saveToCSVOne = function (name, thing2save) {
  const pathToSave = `${createDirSync(path.resolve(config.uploadDir, `csv/${name}`))}${name}.csv`
  csvStringify(thing2save, {}, (err, str) => {
    fs.writeFile(pathToSave, str, 'utf8', function (err) {
      if (err) {
        console.log('Some error occured - file either not saved or corrupted file saved.');
      } else {
        return null
      }
    })
  })
  return null
}