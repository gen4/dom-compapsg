module.exports = {
  apps : [{
    name        : "api-dom-apsg",
    script      : "./server/app.js",
    watch       : false,
    env: {
      "NODE_ENV": "production",
      "PORT":5030,
      "APP_HOST": "127.0.0.1", //"vote.dom-competition.ru",
      "MONGODB_URI": "mongodb://127.0.0.1:27017/dom-apsg-new"
    }
  }]
}