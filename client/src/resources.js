import Vue from 'vue'

// things resource
export const thing = Vue.resource('things{/_id}', {}, {
  like: { method: 'POST', url: 'things{/id}/like' },
  showPrev: { method: 'GET', url: 'things{/id}/prev' },
  showNext: { method: 'GET', url: 'things{/id}/next' }
})

// users resource
export const user = Vue.resource('users{/_id}', {}, {
  changePassword: { method: 'PUT', url: 'users{/id}/password' },
  performance: { method: 'GET', url: 'users/me/performance' }
})

// users resource
export const project = Vue.resource('projects{/_id}', {}, {
  saveVis: { method: 'POST', url: 'projects{/_id}/vis' },
  removeVis: { method: 'DELETE', url: 'projects{/_id}/vis{/visId}' },
  updateVis: { method: 'PUT', url: 'projects{/_id}/vis{/visId}' },
  prolongDeadline: { method: 'POST', url: 'projects{/_id}/deadline' },
  download: { method: 'POST', url: 'projects{/_id}/download' },
  csv: { method: 'POST', url: 'projects{/_id}/csv' },
  assignID4: { method: 'POST', url: 'projects{/_id}/assignID4' },
  showNext: { method: 'GET', url: 'projects{/_id}/showNext' },
  showPrev: { method: 'GET', url: 'projects{/_id}/showPrev' },
  like: { method: 'POST', url: 'projects{/_id}/like' },
  getLike: { method: 'GET', url: 'projects{/_id}/like' }
})

export const me = Vue.resource('users/me', {}, {
  getProject: { method: 'GET', url: 'users/me/project' },
  saveProject: { method: 'POST', url: 'users/me/project' },
  updateProject: { method: 'PUT', url: 'users/me/project' },
  // saveVis: { method: 'POST', url: 'users/me/projects{/projectId}/vis' },
  // removeVis: { method: 'DELETE', url: 'users/me/projects{/projectId}/vis{/_id}' },
  // updateVis: { method: 'PUT', url: 'users/me/projects{/projectId}/vis{/_id}' },
  deleteProject: { method: 'DELETE', url: 'users/me/project/vis{/_id}' },
  submitProject: { method: 'POST', url: 'users/me/project/submit' },
  tokenCount: { method: 'POST', url: 'users/me/token-count' }
})

// upload resource
export const vis = Vue.resource('vis{/_id}', {}, {
  removeUpload: { method: 'DELETE', url: 'vis{/_id}/uploads{/uploadId}' },
  updateUpload: { method: 'PUT', url: 'vis{/_id}/uploads' }
})

// upload resource
export const upload = Vue.resource('uploads{/_id}')

// sid resource
export const sid = Vue.resource('sids{/_id}')

// image resource
export const images = Vue.resource('images{/_id}')

// image resource
export const checkers = Vue.resource('checkers{/_id}')

// like resourses
export const likes = Vue.resource('likes', {}, {
  groupedList: { method: 'GET', url: 'likes/group' }
})

// stage resource
export const stages = Vue.resource('stages{/_id}', {}, {
  update: { method: 'PATCH', url: 'stages{/_id}' },
  currentThings: { method: 'GET', url: 'stages/currentThings' },
  top100: { method: 'GET', url: 'stages/top100' },
  currentLikes: { method: 'GET', url: 'stages/currentLikes' },
  finalize: { method: 'POST', url: 'stages/finalizeCurrent' },
  setAsCurrent: { method: 'POST', url: 'stages/setAsCurrent' }
})

// system resource
export const system = Vue.resource('system', {}, {
  setCurrentStage: { method: 'POST', url: 'system/currentStage' },
  bar: { method: 'POST', url: 'someItem/bar{/id}' },
  allProjectsCsv: { method: 'GET', url: 'system/allProjectsCsv' },
  totalConverts: { method: 'GET', url: 'system/totalConverts' },
  totalConvertsById: { method: 'GET', url: 'system/totalConverts{/_id}' },
  switchCurrents: { method: 'POST', url: 'system/switchCurrents' }
})
