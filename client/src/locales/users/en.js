export default {
  user: {
    breadcrumb: {
      home: 'Home',
      current: 'Users'
    },
    model: {
      username: 'Username',
      role: 'Role',
      password: 'Password',
      type: 'Type',
      id6: 'ID6',
      email: 'email'
    },
    create: {
      title: 'Create a user'
    },
    rules: {
      username: 'Please enter the username',
      password: 'Please enter the password',
      id6: 'Please enter the ID6',
      type: 'Please enter the type',
      email: 'Please enter email'
    },
    action: {
      userExisted: 'User existed'
    }
  }
}
