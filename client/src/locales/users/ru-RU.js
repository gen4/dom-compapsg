export default {
  user: {
    breadcrumb: {
      home: 'Домой',
      current: 'Пользователи'
    },
    model: {
      username: 'Имя',
      role: 'Роль',
      password: 'Пароль',
      type: 'Тип',
      id6: 'ID6',
      email: 'email'
    },
    create: {
      title: 'Создать пользователя'
    },
    rules: {
      username: 'Введите имя',
      password: 'Введите пароль',
      id6: 'Введите ID6',
      type: 'Введите тип пользователя',
      email: 'Введите email'
    },
    action: {
      userExisted: 'Пользователь уже существует'
    }
  }
}
