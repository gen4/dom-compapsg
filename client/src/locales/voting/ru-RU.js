export default {
  voting: {
    model: {
      capacity: 'Кол-во работ',
      order: 'Порядковый номер'
    },
    create: {
      title: 'Создать этап'
    },
    edit: {
      title: 'Редактировать этап'
    }
  }
}
