export default {
  project: {
    submit: 'Submit',
    submitError: 'Project submit error. Check if all required fields are filled',
    submitSuccess: 'Congratulations!\n' +
    '\n' +
    'Your Competition proposal has been successfully submitted! It will be evaluated by the Jury after the submission deadline. \n' +
    '\n' +
    'Also we would like to inform you that the public vote will start shortly. We would like to remind you that the voting process is supposed to be anonymous thus we kindly ask you not to disclose an authorship of your project, publish it on social networks and encourage people to vote for it. Hope for your understanding!\n' +
    '\n' +
    'Thank you and good luck!\n',
    shouldBeFilled: 'Submission is possible only after filling in all the required fields. Please finish to fill in the data.',
    desc: 'Concept description',
    name: 'Name',
    album: 'Album',
    panel1: 'Presentation Panel 1',
    panel2: 'Presentation Panel 2',
    panel1Print: 'Presentation Panel 1',
    panel2Print: 'Presentation Panel 2',
    decl: 'Declaration of authorship',
    consent: 'Letter of consent to the competition documentation',
    note: 'Explanatory note',
    schemas: 'Scheme',
    blueprint: 'Drawings',
    items: 'Content',
    vis: 'Visualizations',
    versionDigital: 'Digital version',
    versionPrint: 'Print version',
    addVis: 'Add visualization',
    visRequired: 'It is required to upload no less than 5 images',
    upto: 'Up to',
    files: 'files',
    max: 'Max',
    min: 'Min',
    mb: 'Mb',
    userError: {
      part1: 'Oops! An error has occurred. Please try to reload the page. If this doesn’t help, please contact us via',
      part2: 'We kindly ask you to  indicate your 6-digit ID number in the letter title.'
    },
    oneFile: '1 file only',
    subItemChoice: 'Please choose 3 out of 4 types of buildings. You will be able to change your choice whenever you wish before the final submission.',
    panic: 'Dear Participants!\n' +
    '\n' +
    'The possibility to upload the materials is temporarily unavailable. Soon the technical problems will be solved, and it will be possible to continue filling in the information. \n' +
    '\n' +
    'Thank you for your patience and understanding!\n',
    deadline: {
      part1: 'The submission deadline has passed. If you have faced technical problems while uploading the proposal, we kindly ask you to inform us via email ',
      part2: 'Indicating your 6-digit number of the participant in the letter.',
      part3: 'Thank you!'
    },
    choiceFile: 'Choose file',
    choiceFiles: 'Choose files',
    support: 'Support',
    supportDesc: 'Push this button in case you have faced technical problems uploading your Competition Proposal',
    sizeError: 'The size of the uploaded file exceeds the limit',
    optional: 'optional',
    searchByIdPlaceholder: 'Enter a project ID'
  }
}
