export default {
  pagination: {
    current: 'текущая',
    currentAppend: 'страница',
    pages: 'всего',
    pagesAppend: 'страница'
  }
}
