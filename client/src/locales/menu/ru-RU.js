export default {
  menu: {
    users: 'Пользователи',
    projects: 'Работы',
    voting: 'Голосование',
    templates: 'Шаблоны',
    system: 'Система',
    result: 'Итоги'
  }
}
