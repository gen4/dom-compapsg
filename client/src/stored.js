import { read } from './storage'
import { STORE_KEY_USERNAME, STORE_KEY_ACCESS_TOKEN, STORE_KEY_REFRESH_TOKEN,
  STORE_KEY_CONFIG_LANG, STORE_KEY_CONFIG_PAGE_LIMIT, STORE_HELP_FLAG, STORE_TUTOR_SHOWN,
  STORE_FIRST_COUNT, STORE_SECOND_COUNT, STORE_THIRD_COUNT } from './constants'

export const username = read(STORE_KEY_USERNAME) || ''
export const isTutorShown = read(STORE_TUTOR_SHOWN) || false
// eslint-disable-next-line camelcase
export const access_token = read(STORE_KEY_ACCESS_TOKEN) || '' // eslint-disable-line
// eslint-disable-next-line camelcase
export const refresh_token = read(STORE_KEY_REFRESH_TOKEN) || '' // eslint-disable-line
// lang order: localStorage -> browser language -> default
export const lang = read(STORE_KEY_CONFIG_LANG) || 'ru-RU'
export const pageLimit = +read(STORE_KEY_CONFIG_PAGE_LIMIT) || 20
export const helpFlag = read(STORE_HELP_FLAG) || ''
export const firstCount = read(STORE_FIRST_COUNT)
export const secondCount = read(STORE_SECOND_COUNT)
export const thirdCount = read(STORE_THIRD_COUNT)
