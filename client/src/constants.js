// user info
export const STORE_KEY_USERNAME = 'user.username'
export const STORE_KEY_ACCESS_TOKEN = 'user.access_token'
export const STORE_KEY_REFRESH_TOKEN = 'user.refresh_token'
export const STORE_HELP_FLAG = 'user.help_flag'
export const STORE_TUTOR_SHOWN = 'user.tutor_shown'

export const STORE_FIRST_COUNT = 'user.first_count'
export const STORE_SECOND_COUNT = 'user.second_count'
export const STORE_THIRD_COUNT = 'user.third_count'

// user config
export const STORE_KEY_CONFIG_LANG = 'config.lang'
export const STORE_KEY_CONFIG_PAGE_LIMIT = 'config.page.limit'
