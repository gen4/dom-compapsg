/**
 * App router config
 */
import Vue from 'vue'
import _ from 'lodash'
import VueRouter from 'vue-router'
import store from '../store'
import adminRoutes from './admin'
Vue.use(VueRouter)

const routes = [{
  path: '/login',
  component: (resolve) => {
    import('../view/auth/Login.vue').then(resolve)
  },
  meta: {
    skipAuth: true
  }
}, {
  path: '/logout',
  component: (resolve) => {
    import('../view/auth/Logout.vue').then(resolve)
  },
  meta: {
    skipAuth: true
  }
}, {
  path: '/admin',
  component: (resolve) => {
    import('../view/CommonView.vue').then(resolve)
  },
  meta: {
    roleOnly: 'admin'
  },
  children: [...adminRoutes, {
    path: '/', redirect: '/admin/voting',
    meta: {
      roleOnly: 'admin'
    }
  }]
},
// {
//   path: '/',
//   component: (resolve) => {
//     import('../view/Dummy.vue').then(resolve)
//   },
//   meta: {
//     skipAuth: true
//   }
// },
{
  path: '/result',
  component: (resolve) => {
    import('../view/Result.vue').then(resolve)
  },
  meta: {
    skipAuth: true
  }
},
{
  path: '/cabinet',
  component: (resolve) => {
    import('../view/CabinetSubmit.vue').then(resolve)
  },
  meta: {
    skipAuth: true
  }
},
{
  path: '/tri-button',
  component: (resolve) => {
    import('../view/TestView.vue').then(resolve)
  },
  meta: {
    skipAuth: true
  }
},
{
  path: '/',
  component: (resolve) => {
    import('../view/ThingsSearch.vue').then(resolve)
  }
}, {
  path: '/work/:id',
  component: (resolve) => {
    import('../view/Thing.vue').then(resolve)
  }
}, {
  path: '*',
  component: {
    render (h) {
      return h('div', { staticClass: 'flex flex-main-center', attrs: { style: 'width:100%;font-size:32px' }}, 'Page not found')
    }
  }
}]

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  // scrollBehavior: () => ({ y: 0 }),
  routes
})

export function hook (userPromise) {
  // router
  router.beforeEach((to, from, next) => {
    userPromise.then(() => {
      store.dispatch('changeRouteLoading', true).then(() => {
        // has logged in, reject
        if (to.path === '/login' && store.getters.loggedIn) {
          return next(false)
        }
        const restricted = _.some(to.matched, (m) => {
          console.log('> m.meta.roleOnly', m.meta.roleOnly, m)
          return m.meta.roleOnly && m.meta.roleOnly !== store.getters.userRole
        })
        console.log('R ', restricted)
        if (restricted) {
          console.log('RST')
          next({
            path: '/login',
            query: { redirect: to.fullPath }
          })
        }
        if (!to.meta.skipAuth) {
          // this route requires auth, check if logged in
          // if not, redirect to login page.
          if (!store.getters.loggedIn) {
            console.log('No login')
            next({
              path: '/login',
              query: { redirect: to.fullPath }
            })
          } else {
            next()
          }
        } else {
          next()
        }
      })
    })
  })

  router.afterEach(() => {
    store.dispatch('changeRouteLoading', false)
  })
}

export default router
