import Promise from 'bluebird'

const get = function (url, params, callback) {
  var client = new window.XMLHttpRequest()
  client.onreadystatechange = function () {
    if (client.readyState !== 4) return
    callback(client.response)
  }
  client.withCredentials = true
  const query = params && Object.keys(params).map(function (key) {
    return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
  }).join('&')
  client.open('GET', url + '?' + query, true)
  client.send()
}

function userFetcher () {
  return new Promise((resolve, reject) => {
    const HOST = 'https://dom-competition.ru'
    get(HOST + '/api/getToken', null, function (response) {
      if (response.error) {
        return reject(response)
      }
      return resolve(response)
    })
  })
}
export default userFetcher
